package com.cereslogic.tidecast.data;

import java.util.Date;

import com.cereslogic.tidecast.client.MarineForecast;

public class ResultListForecastHeader extends ResultListItem {
	private String mForecastZone;
	private String mForecastAlert;
	private Date mForecastDate;
	
	public void setZoneDescription(String forecastZone) {	
		mForecastZone = forecastZone; 
	}
	public String getZoneDescription() { 
		return mForecastZone; 
	}
	
	public String getForecastZone() {
		return this.mForecastZone;
	}	
	public void setForecastZone(String forecastZone) {
		this.mForecastZone = forecastZone;
	}

	public String getForecastAlert() {
		return this.mForecastAlert;
	}
	public void setForecastAlert(String forecastAlert) {
		this.mForecastAlert = forecastAlert;
	}
	
	public Date getForecastDate() {
		return this.mForecastDate;
	}
	public void setForecastDate(Date forecastDate) {
		this.mForecastDate = forecastDate;
	}
	
	public ResultListForecastHeader(MarineForecast marineForecast) {
		setZoneDescription(marineForecast.getZoneDescription());
		setForecastAlert(marineForecast.getAlert());
		setForecastDate(marineForecast.getForecastDate());
	}
}
