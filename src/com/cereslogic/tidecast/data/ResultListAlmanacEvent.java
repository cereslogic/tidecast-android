package com.cereslogic.tidecast.data;

import android.graphics.Bitmap;

public class ResultListAlmanacEvent extends ResultListItem {
	private String mLabel;
	private String mValue;
	private Bitmap mIcon;
	private boolean mClickable;
	
	public boolean isClickable() {
		return this.mClickable;
	}
	public void setClickable(boolean clickable) {
		this.mClickable = clickable;
	}
	public String getLabel() {
		return mLabel;
	}
	public void setLabel(String label) {
		this.mLabel = label;
	}
	public String getValue() {
		return mValue;
	}
	public void setValue(String value) {
		this.mValue = value;
	}
	public Bitmap getIcon() {
		return mIcon;
	}
	public void setIcon(Bitmap icon) {
		this.mIcon = icon;
	}
	
	public ResultListAlmanacEvent(String label, String value, Bitmap icon, boolean clickable) {
		super();
		this.mLabel = label;
		this.mValue = value;
		this.mIcon = icon;
		this.mClickable = clickable;
	}
}
