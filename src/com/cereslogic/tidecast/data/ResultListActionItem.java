package com.cereslogic.tidecast.data;

public class ResultListActionItem extends ResultListItem {
	private String action;
	private String altAction;
	private boolean actionEnabled = true;
	private boolean altActionEnabled = true;
	
	public ResultListActionItem(String action) {
		super();
		this.action = action;
	}
	
	public ResultListActionItem(String action, boolean actionEnabled) {
		super();
		this.action = action;
		this.actionEnabled = actionEnabled;
	}	
	
	public ResultListActionItem(String action, String altAction) {
		super();
		this.action = action;
		this.altAction = altAction;
	}	
	
	public ResultListActionItem(String action, String altAction, boolean actionEnabled, boolean altActionEnabled) {
		super();
		this.action = action;
		this.altAction = altAction;
		this.actionEnabled = actionEnabled;
		this.altActionEnabled = altActionEnabled;
	}	
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getAltAction() {
		return altAction;
	}
	public void setAltAction(String altAction) {
		this.altAction = altAction;
	}

	public boolean isActionEnabled() {
		return actionEnabled;
	}
	public void setActionEnabled(boolean actionEnabled) {
		this.actionEnabled = actionEnabled;
	}

	public boolean isAltActionEnabled() {
		return altActionEnabled;
	}
	public void setAltActionEnabled(boolean altActionEnabled) {
		this.altActionEnabled = altActionEnabled;
	}
}
