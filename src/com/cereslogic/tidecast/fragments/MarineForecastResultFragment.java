/*
 * Copyright 2010 Ceres Logic LLC.  All Rights Reserved.
 * @author Mike Desjardins
 */
package com.cereslogic.tidecast.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.MarineForecast;
import com.cereslogic.tidecast.client.MarineForecastClient;
import com.cereslogic.tidecast.client.TidecastClientException;
import com.cereslogic.tidecast.data.ResultListActionItem;
import com.cereslogic.tidecast.data.ResultListForecastFooter;
import com.cereslogic.tidecast.data.ResultListForecastHeader;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.ui.MarineForecastListAdapter;
import com.cereslogic.tidecast.utils.Prefs;

public class MarineForecastResultFragment extends SherlockListFragment {
	private Station mStation;
	private ProgressDialog mProgressDialog = null;
	private Prefs mPrefs;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	  Intent intent = getActivity().getIntent();
	  mStation = (Station)intent.getExtras().getParcelable("station");
		mPrefs = new Prefs(getActivity());
		getActivity().setTitle(mStation.getPlace());
		
	  new FetchTidesTask().execute();
	}	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
			    DialogFragment newFragment = new DatePickerFragment();
			    Bundle bundle = new Bundle();
			    bundle.putParcelable("station", mStation);
			    newFragment.setArguments(bundle);
			    newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
				}
			}
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	getActivity().finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
	
	@Override 
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		menuInflater.inflate(R.menu.show_tides, menu);
		super.onCreateOptionsMenu(menu, menuInflater);
	}
	
	private class FetchTidesTask extends AsyncTask</*Params*/ Void, /* Progress */ Void, /* Result */ MarineForecast> {
    protected MarineForecast doInBackground(Void... nil) {
    	MarineForecastClient marineForecastClient = new MarineForecastClient();
    	MarineForecast marineForecast = new MarineForecast();
  	  try {
				marineForecast = marineForecastClient.getForecast(mStation);
			} catch (TidecastClientException e) {
				Toast.makeText(getActivity(), R.string.error_contacting_noaa, Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
			return marineForecast;
    }
    
    protected void onPostExecute(MarineForecast marineForecast) {
    	if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			Resources resources = getActivity().getResources();
			List<Object> items = new ArrayList<Object>();
			
			items.add(new ResultListForecastHeader(marineForecast));
			items.addAll(marineForecast.getDailyForecast());
			items.add(new ResultListForecastFooter(marineForecast));
			
			String altActionButtonText = mPrefs.isFavorite(mStation.getId()) ? 
					resources.getString(R.string.remove_favorite) : resources.getString(R.string.add_to_favorites);
			ResultListActionItem ai = new ResultListActionItem("Show in Widget",altActionButtonText);
			ai.setActionEnabled(!mPrefs.isWidgetStation(mStation.getId()));
			items.add(ai);
      setListAdapter(new MarineForecastListAdapter(getActivity(), items));
    }
	}
}
