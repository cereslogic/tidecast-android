/*
 * Copyright 2010 Ceres Logic LLC.  All Rights Reserved.
 * @author Mike Desjardins
 */
package com.cereslogic.tidecast.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.SunriseSunsetCalculator;
import com.cereslogic.tidecast.client.Tide;
import com.cereslogic.tidecast.client.TideForecastClient;
import com.cereslogic.tidecast.client.TidecastClientException;
import com.cereslogic.tidecast.data.ResultListActionItem;
import com.cereslogic.tidecast.data.ResultListAlmanacEvent;
import com.cereslogic.tidecast.data.ResultListItem;
import com.cereslogic.tidecast.factories.MoonPhaseEventFactory;
import com.cereslogic.tidecast.factories.SunriseSunsetEventFactory;
import com.cereslogic.tidecast.factories.TideEventFactory;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.ui.AlmanacEventListAdapter;
import com.cereslogic.tidecast.utils.Prefs;
import com.cereslogic.tidecast.utils.TimeZoneOffsetCalculator;

public class TideResultFragment extends SherlockListFragment {
	private Station mStation;
	private Date mDay;
	private ProgressDialog mProgressDialog = null;
	private Prefs mPrefs;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	  Intent intent = getActivity().getIntent();
	  mStation = (Station)intent.getExtras().getParcelable("station");
	  mDay = (Date)intent.getExtras().get("day");
		mPrefs = new Prefs(getActivity());
		getActivity().setTitle(mStation.getPlace());
		
	  new FetchTidesTask().execute();
	}	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
			    DialogFragment newFragment = new DatePickerFragment();
			    Bundle bundle = new Bundle();
			    bundle.putParcelable("station", mStation);
			    newFragment.setArguments(bundle);
			    newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
				}
			}
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	getActivity().finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
	
	@Override 
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		menuInflater.inflate(R.menu.show_tides, menu);
		super.onCreateOptionsMenu(menu, menuInflater);
	}
	
	private class FetchTidesTask extends AsyncTask</*Params*/ Void, /* Progress */ Void, /* Result */ List<Tide>> {
    protected List<Tide> doInBackground(Void... nil) {
    	TideForecastClient tideForecastClient = new TideForecastClient();
    	List<Tide> result = new ArrayList<Tide>();
  	  try {
				result = tideForecastClient.getTides(mStation,mDay,mDay);
			} catch (TidecastClientException e) {
				getActivity().runOnUiThread(new Runnable() {
				  public void run() {
				  	Toast.makeText(getActivity(), R.string.error_contacting_noaa, Toast.LENGTH_SHORT).show();
				  }
				});
				e.printStackTrace();
			}
			return result;
    }
    
    protected void onPostExecute(List<Tide> tides) {
    	if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			Resources resources = getActivity().getResources();
			List<ResultListItem> items = new ArrayList<ResultListItem>();
			items.add(createAlamanacEventForDate());
			items.addAll(TideEventFactory.makeTides(tides, resources));
			int offset = TimeZoneOffsetCalculator.getTimeZoneOffset(mStation.getPlace(), mStation.getState(), mDay);
			SunriseSunsetCalculator ss = new SunriseSunsetCalculator(mStation.getLatitude(), mStation.getLongitude(), mDay, offset);				
			ResultListAlmanacEvent sunrise = SunriseSunsetEventFactory.makeSunrise(ss, resources); 
			if (sunrise != null) {
				items.add(sunrise);
			}
			ResultListAlmanacEvent sunset = SunriseSunsetEventFactory.makeSunset(ss, resources);
			if (sunset != null) {
				items.add(sunset);
			}				
			ResultListAlmanacEvent moon = MoonPhaseEventFactory.makeMoonPhase(TideResultFragment.this.mDay, resources);
			if (moon != null) {
				items.add(moon);
			}
			
			String altActionButtonText = mPrefs.isFavorite(mStation.getId()) ? 
					resources.getString(R.string.remove_favorite) : resources.getString(R.string.add_to_favorites);
			ResultListActionItem ai = new ResultListActionItem("Show in Widget",altActionButtonText);
			ai.setActionEnabled(!mPrefs.isWidgetStation(mStation.getId()));
			items.add(ai);
      setListAdapter(new AlmanacEventListAdapter(getActivity(), items));
    }
	}
	
	private ResultListAlmanacEvent createAlamanacEventForDate() {
		return new ResultListAlmanacEvent(
				              getResources().getString(R.string.tap_to_change), 
				              new SimpleDateFormat("EEEE, MMMM d", Locale.ENGLISH).format(mDay),
				              BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.calendar),
				              true);
	}
}
