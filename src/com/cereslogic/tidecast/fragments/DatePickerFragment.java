package com.cereslogic.tidecast.fragments;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.cereslogic.tidecast.persist.Station;

// TODO - This isn't really "reusable" enough... it really only works w/ the
// TideResultFragment, which is fine for now.
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
	private Station mStation;
	
	@Override
	public void setArguments(Bundle bundle) {
		this.mStation = bundle.getParcelable("station");
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		// Do something with the date chosen by the user
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, monthOfYear);
		cal.set(Calendar.DATE, dayOfMonth);
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.putExtra("station", (Parcelable)mStation);
    intent.putExtra("day", cal.getTime());
    intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultContainerActivity"));
    startActivity(intent);
	}
}
