package com.cereslogic.tidecast.persist;

import android.os.Parcel;
import android.os.Parcelable;


public class StateOrRegion extends IdObject implements Parcelable {
	private String mText;
	private Integer mId;

	public StateOrRegion(String text, Integer id) {
		super();
		this.mText = text;
		this.mId = id;
	}

  public void writeToParcel(Parcel out, int flags) {  
		out.writeInt(mId);
		out.writeString(mText);
  }  
	
	private void readFromParcel(Parcel in) {
		mId = in.readInt();
		mText = in.readString();
  }  
  
  public StateOrRegion(Parcel in) {  
    readFromParcel(in);  
   }
  
  public static final Parcelable.Creator<StateOrRegion> CREATOR = new Parcelable.Creator<StateOrRegion>() {  
    public StateOrRegion createFromParcel(Parcel in) {  
    	return new StateOrRegion(in);  
    }  
    public StateOrRegion[] newArray(int size) {  
    	return new StateOrRegion[size];  
    }  
  };  
  
  public int describeContents() {  
    return this.hashCode();  
  } 

	public String getText() {
		return this.mText;
	}
	public void setText(String text) {
		this.mText = text;
	}
	public Integer getId() {
		return this.mId;
	}
	public void setId(Integer id) {
		this.mId = id;
	}	
}
