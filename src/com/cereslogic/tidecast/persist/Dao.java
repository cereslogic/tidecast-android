/*
 * Copyright 2010 Ceres Logic LLC.  All Rights Reserved.
 * @author Mike Desjardins
 */
package com.cereslogic.tidecast.persist;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.SparseArray;

import com.cereslogic.tidecast.utils.Logger;

public class Dao extends SQLiteOpenHelper implements Serializable {
	private static final long serialVersionUID = -1386887430960146411L;

	private final static String DB_NAME = "stations";
	private final static String DB_PATH = "/data/data/com.cereslogic.tidecast/databases";
	private final static int DB_VERSION = 1;
	private final static int ARRAY_LIST_SIZE = 50;
	private static SQLiteDatabase db;	
	private static Dao me = null;
	private static SparseArray<String> states = new SparseArray<String>(40);
	private static Map<String,Integer> stateIds = new HashMap<String,Integer>(40);
	private static SparseArray<String> regions = new SparseArray<String>(200);
	
	// This has to map to the createStation method.
	private final static String[] STATION_COLUMNS =	{
		"station_code", 			//0
    "sec_station_code",	  //1
    "longitude",					//2
    "latitude",					  //3
    "thh",								//4
    "thm",								//5
    "ths",								//6
    "tlh",								//7
    "tlm",								//8
    "tls",								//9
    "hh",								  //10
    "hl",								  //11
    "legacy_id",					//12
    "region_id",					//13
    "id",								  //14
    "zone",							  //15
    "legacy_id",					//16
    "enabled",						//17
    "state_id",					  //18
    "name"								//19
  };

	@Override
	public void onCreate(SQLiteDatabase db) {	}
		
	@Override 
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}
		
	@Override 
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
		
	public static synchronized Dao getInstance(Context context) {
		if (Dao.me == null) {
			Dao.me = new Dao(context);
		}
		return me;
	}
	
	public Object clone()	throws CloneNotSupportedException  {
    throw new CloneNotSupportedException(); 
  }
	
	public static boolean isInitialized() {
		return Dao.me != null;
	}
	
	protected Dao(Context context) {
		super(context, Dao.DB_NAME, null, Dao.DB_VERSION);
		InputStream assetsDB = null;
		try {
			assetsDB = context.getAssets().open(DB_NAME);
			File f = new File(DB_PATH);
			if (!f.exists()) {
				f.mkdirs();
			}
			OutputStream dbOut = new FileOutputStream(DB_PATH + "/" + DB_NAME);

			byte[] buffer = new byte[1024];
			int length;
			while ((length = assetsDB.read(buffer)) > 0) {
				dbOut.write(buffer, 0, length);
			}

			dbOut.flush();
			dbOut.close();
			assetsDB.close();
			Logger.i("New database created...");
			
			Dao.db = getWritableDatabase();
		} catch (IOException e) {
			Logger.e("Could not create new database...");
			e.printStackTrace();
		}
		Logger.d("Done.");
	}
	
	public static String getAbbrev(String stateName) {
		//Logger.d("getAbbrev(" + stateName + ")");
		String[] cols = {"abbrev"};
		String[] params = {stateName};
		String result = "";
		Cursor c = null;
		try {
			c = Dao.db.query("states", cols, "full = ?", params, null, null, null, "1");
			c.moveToFirst();
			result = c.getString(0);
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}
	
	public static String getStateName(String abbrev) {
		//Logger.d("getStateName(" + abbrev + ")");
		String[] cols = {"full"};
		String[] params = {abbrev};
		String result = "";
		Cursor c = null;
		try {
			c = Dao.db.query("states", cols, "abbrev = ?", params, null, null, null, "1");
			c.moveToFirst();
			result = c.getString(0);
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}	
	
	public static Integer getStateId(String abbrev) {
		//Logger.d("getStateId(" + abbrev + ")");
		Integer result = Dao.stateIds.get(abbrev);
		if (abbrev == null) {
			//Logger.d("Cache Miss.");
			String[] cols = {"abbrev","id"};
			String[] params = {abbrev};
			Cursor c = null;
			try {
				c = Dao.db.query("states", cols, "abbrev = ?", params, null, null, null, "1");		
				c.moveToFirst();
				result = c.getInt(1);
				Dao.stateIds.put(abbrev, result);
			} catch (SQLException e) {
				Logger.e("Caught an exception querying database.", e);
			} finally {
				if (c != null && !c.isClosed()) {
					c.close();
				}
			}
		}
		return result;
	}
	
	public static String getState(Integer stateId) {
		//Logger.d("getState(" + stateId + ")");
		String result = Dao.states.get(stateId);
		if (result == null) {
			//Logger.d("Cache Miss.");
			String[] cols = {"id","abbrev"};
			String[] params = {stateId.toString()};
			Cursor c = null;
			try {
				c = Dao.db.query("states", cols, "id = ?", params, null, null, null, "1");		
				c.moveToFirst();
				result = c.getString(1);
				Dao.states.put(stateId, result);
			} catch (SQLException e) {
				Logger.e("Caught an exception querying database.", e);
			} finally {
				if (c != null && !c.isClosed()) {
					c.close();
				}
			}
		} 
		return result;
	}
	
	public static String getRegion(Integer regionId) {
		//Logger.d("getRegion(" + regionId + ")");
		String result = Dao.regions.get(regionId);
		if (result == null) {
			//Logger.d("Cache Miss.");
			String[] cols = {"name"};
			String[] params = {regionId.toString()};
			Cursor c = null;
			try {
				c = Dao.db.query("regions", cols, "id = ?", params, null, null, null, "1");
				c.moveToFirst();
				result = c.getString(0);
				Dao.regions.put(regionId, result);
			} catch (SQLException e) {
				Logger.e("Caught an exception querying database.", e);
			} finally {
				if (c != null && !c.isClosed()) {
					c.close();
				}
			}
		}
		return result;
	}
	
	public ArrayList<StateOrRegion> getStatesOrRegions() {
		//Logger.d("getStatesOrRegions()");
		ArrayList<StateOrRegion> result = new ArrayList<StateOrRegion>();
		String[] cols = {"full","id"};		
		Cursor c = null;
		try {
			c = Dao.db.query("states", cols, null, null, null, null, "full");			
			c.moveToFirst();
			int numRows = c.getCount();
			for (int i=0; i<numRows; ++i) {
				result.add(new StateOrRegion(c.getString(0),c.getInt(1)));
				c.moveToNext();			
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}
		
	public ArrayList<Subregion> getSubregionsForState(Integer stateId) {
		//Logger.d("getSubregionsForState(" + stateId + ")");
		ArrayList<Subregion> result = new ArrayList<Subregion>(Dao.ARRAY_LIST_SIZE);
		String[] cols = {"name","id"};
		String[] params = {stateId.toString()}; 
		Cursor c = null;
		try {
			c = Dao.db.query("regions", cols, "state_id = ?", params, null, null, "name");			
			c.moveToFirst();
			int numRows = c.getCount();
			for (int i=0; i<numRows; ++i) {
				result.add(new Subregion(c.getString(0),c.getInt(1)));
				c.moveToNext();
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}
	
	public ArrayList<Station> getStationsForState(Integer stateId, String sort) {
		//Logger.d("getStationsForState(" + stateId + "," + sort + ")");
		ArrayList<Station> result = new ArrayList<Station>(Dao.ARRAY_LIST_SIZE);		
		String[] params = {stateId.toString(), "1"}; 
		Cursor c = null;
		try {
			c = Dao.db.query("stations", STATION_COLUMNS, "state_id = ? and enabled = ?", params, null, null, sort);			
			c.moveToFirst();
			int numRows = c.getCount();
			for (int i=0; i<numRows; ++i) {
				result.add(createStation(c));
				c.moveToNext();
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}
	
	public ArrayList<Station> getStationsForSubregion(Integer subregionId, String sort) {
		//Logger.d("getStationsForSubregion(" + subregionId + "," + sort + ")");
		ArrayList<Station> result = new ArrayList<Station>(Dao.ARRAY_LIST_SIZE);
		String[] params = {subregionId.toString(), "1"}; 
		Cursor c = null;
		try {
			c = Dao.db.query("stations", Dao.STATION_COLUMNS, "region_id = ? and enabled = ?", params, null, null, sort);
			c.moveToFirst();
			int numRows = c.getCount();
			for (int i=0; i<numRows; ++i) {
				result.add(createStation(c));
				c.moveToNext();
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}
	
	public Station getStation(String state, String place) {
		//Logger.d("getStation(" + place + "," + state + ")");
		Integer state_id = getStateId(state);
		String[] params = {state_id.toString(), place}; 
		Cursor c = null;
		Station result = null;
		try {
			c = Dao.db.query("stations", Dao.STATION_COLUMNS, "state_id = ? and name = ?", params, null, null, "name");
			c.moveToFirst();
			result = createStation(c);
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}

	public Station getStation(String shortName) {
		//Logger.d("getStation(" + shortName + ")");
		String state = shortName.substring(shortName.lastIndexOf('(')+1,shortName.lastIndexOf('(')+3);
		String place = shortName.substring(0,shortName.lastIndexOf('(')-1);
		return getStation(state,place);
	}
	
	public Station getStationByNwsId(String legacyId) {
		//Logger.d("getStationByNwsId(" + legacyId + ")");
		String[] params = {legacyId}; 
		Station result = null;
		Cursor c = null;
		try {
			c = Dao.db.query("stations", Dao.STATION_COLUMNS, "legacy_id = ?", params, null, null, "1");
			if (c.getCount() > 0) {
				c.moveToFirst();
				result = createStation(c);
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;
	}
	
	public Station getStation(Integer stationId) {	
		//Logger.d("getStation(" + stationId + ")");
		String[] params = {stationId.toString()}; 
		Cursor c = null;
		Station result = null;
		try {
			c = Dao.db.query("stations", Dao.STATION_COLUMNS, "id = ?", params, null, null, "name");
			c.moveToFirst();
			result = createStation(c);
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;		
	}
	
	// Used by unit tests. Probably don't want to call this in real life!
	public List<Integer> getAllStationIds() {
		List<Integer> result = new ArrayList<Integer>(Dao.ARRAY_LIST_SIZE);
		String[] cols = {"id"};
		Cursor c = Dao.db.query("stations", cols, "enabled=1", null, null, null, null);
		if (c.getCount() > 0) {
			c.moveToFirst();
			int numRows = c.getCount();
			for (int i=0; i<numRows; ++i) {
				result.add(c.getInt(0));
				c.moveToNext();
			}
		}
		return result;
	}
	
	public static List<Station> getAllStations() {
		List<Station> result = new ArrayList<Station>(Dao.ARRAY_LIST_SIZE);
		Cursor c = null;
		try {
			c = Dao.db.query("stations", Dao.STATION_COLUMNS, "enabled=1", null, null, null, null);
			if (c.getCount() > 0) {
				c.moveToFirst();
				int numRows = c.getCount();
				for (int i=0; i<numRows; ++i) {
					result.add(createStation(c));
					c.moveToNext();
				}
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;		
	}
	
	public static List<Station> getStationsInArea(Double topLeftLongitude, Double topLeftLatitude, Double bottomRightLongitude, Double bottomRightLatitude) {
		//Logger.d("getStationsInArea(" + topLeftLongitude + "," + topLeftLatitude + "," + bottomRightLongitude + "," + bottomRightLatitude + ")");
		List<Station> result = new ArrayList<Station>(Dao.ARRAY_LIST_SIZE);
		Cursor c = null;
		try {
			String query = "enabled=1 and ";
			String[] params = new String[4];
			if (topLeftLongitude > bottomRightLongitude) {
				query += "((longitude > ? or longitude < ?) and latitude < ? and latitude > ?)";
				params[0] = topLeftLongitude.toString();
				params[1] = bottomRightLongitude.toString();
				params[2] = topLeftLatitude.toString();
				params[3] = bottomRightLatitude.toString();
			} else {
				query += "(longitude > ? and latitude < ? and longitude < ? and latitude > ?)";
				params[0] = topLeftLongitude.toString();
				params[1] = topLeftLatitude.toString();
				params[2] = bottomRightLongitude.toString();
				params[3] = bottomRightLatitude.toString();
			}
			
			c = Dao.db.query("stations", Dao.STATION_COLUMNS, query, params, null, null, null);
			if (c.getCount() > 0) {
				c.moveToFirst();
				int numRows = c.getCount();
				for (int i=0; i<numRows; ++i) {
					result.add(createStation(c));
					c.moveToNext();
				}
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		return result;		
	}
	
	public boolean hasSubregions(Integer stateId) {
		//Logger.d("hasSubregions(" + stateId + ")");
		String[] cols = {"name"};
		String[] params = {stateId.toString()}; 
		Cursor c = Dao.db.query("regions", cols, "state_id = ?", params, null, null, null);
		int numRows = c.getCount();
		c.close();
		return numRows > 0;
	}
	
	public Station findClosestStation(Double longitude, Double latitude) {
		//Logger.d("findClosestStation(" + longitude + "," + latitude + ")");
		double shortest = Double.MAX_VALUE;
		Integer closestStationId = null;
		Cursor c = null;
		String[] cols = {"id","longitude","latitude"};
		try {
			c = Dao.db.query("stations", cols, "enabled = 1", null, null, null, null);
			c.moveToFirst();
			int numRows = c.getCount();
			for (int i=0; i<numRows; ++i) {
				float distance = calculateDistance(longitude,latitude,c.getDouble(1),c.getDouble(2));
				if (Math.abs(distance) < shortest) {
					shortest = Math.abs(distance);
					closestStationId = c.getInt(0);
				}
				c.moveToNext();
			}
		} catch (SQLException e) {
			Logger.e("Caught an exception querying database.", e);
		} finally {
			if (c != null && !c.isClosed()) {
				c.close();
			}
		}
		Logger.d("Closest station has ID " + closestStationId);
		return getStation(closestStationId);
	}
	
	private float calculateDistance(Double lon1, Double lat1, Double lon2, Double lat2) {
		float[] results = new float[5];
		Location.distanceBetween(lat1, lon1, lat2, lon2, results);
		return results[0];
	}	
	
	private static Station createStation(Cursor c) {
		String stn = c.getString(0);
		String secStn = c.getString(1);
		Double longitude = c.getDouble(2);
		Double latitude = c.getDouble(3);
		String thh = c.getString(4);
		String thm = c.getString(5);
		String ths = c.getString(6);
		String tlh = c.getString(7);
		String tlm = c.getString(8);
		String tls = c.getString(9);
		String hh = c.getString(10);
		String hl = c.getString(11);
		String subregion = c.isNull(13) ? null : Dao.getRegion(c.getInt(13));
		Integer id = c.getInt(14);
		String zone = c.getString(15);
		String nwsId = c.getString(16);
		Integer enabled = c.getInt(17);
		String state = Dao.getState(c.getInt(18));
		String place = c.getString(19);
		return new Station(id, state, place, subregion, stn,	secStn, thh, 
				thm, ths, tlh, tlm, tls, hh, hl, latitude, longitude, zone, nwsId, 
				enabled);
		
	}
}
