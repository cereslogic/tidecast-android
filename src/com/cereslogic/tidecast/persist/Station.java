package com.cereslogic.tidecast.persist;

import java.util.Locale;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;


public class Station extends IdObject implements Comparable<Station>, Parcelable {
	private Integer id;
	private String state;
	private String subregion;
	private String place;
	private String stationId;
	private String secStationId;
	private Double latitude;
	private Double longitude;
	private String highOffsetHours;
	private String highOffsetMins;
	private String highOffsetSign;
	private String lowOffsetHours;
	private String lowOffsetMins;
	private String lowOffsetSign;
	private String highOffsetFeet;
	private String lowOffsetFeet;
	private String zone;
	private String nwsOffice;
	private String nwsId;
	private Integer enabled;
	
  public void writeToParcel(Parcel out, int flags) {  
		out.writeInt(id);
		out.writeString(state);
		out.writeString(subregion);
		out.writeString(place);
		out.writeString(stationId);
		out.writeString(secStationId);
		out.writeDouble(latitude);
		out.writeDouble(longitude);
		out.writeString(highOffsetHours);
		out.writeString(highOffsetMins);
		out.writeString(highOffsetSign);
		out.writeString(lowOffsetHours);
		out.writeString(lowOffsetMins);
		out.writeString(lowOffsetSign);
		out.writeString(highOffsetFeet);
		out.writeString(lowOffsetFeet);
		out.writeString(zone);
		out.writeString(nwsOffice);
		out.writeString(nwsId);
		out.writeInt(enabled);
  }  
	
	private void readFromParcel(Parcel in) {
		id = in.readInt();
		state = in.readString();
		subregion = in.readString();
		place = in.readString();
		stationId = in.readString();
		secStationId = in.readString();
		latitude = in.readDouble();
		longitude = in.readDouble();
		highOffsetHours = in.readString();
		highOffsetMins = in.readString();
		highOffsetSign = in.readString();
		lowOffsetHours = in.readString();
		lowOffsetMins = in.readString();
		lowOffsetSign = in.readString();
		highOffsetFeet = in.readString();
		lowOffsetFeet = in.readString();
		zone = in.readString();
		nwsOffice = in.readString();
		nwsId = in.readString();
		enabled = in.readInt();  	
  }  
  
  public Station(Parcel in) {  
    readFromParcel(in);  
   }
  
  public static final Parcelable.Creator<Station> CREATOR = new Parcelable.Creator<Station>() {  
    public Station createFromParcel(Parcel in) {  
    	return new Station(in);  
    }  
    public Station[] newArray(int size) {  
    	return new Station[size];  
    }  
  };  
  
  public int describeContents() {  
    return this.hashCode();  
  } 
  
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getText() {
		return this.getPlace();
	}
	
	public String getStationId() {
		return state + "::" + subregion + "::" + place;
	}
//	public void setStationId(String stationId) {
//		this.stationId = stationId;
//	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public String getShortName() {
		return this.place + " (" + this.state + ")";
	}
	
	public String getSubregion() {
		return subregion;
	}
	public void setSubregion(String region) {
		this.subregion = region;
	}

	public String getSecStationId() {
		return secStationId;
	}
	public void setSecStationId(String secStationId) {
		this.secStationId = secStationId;
	}
	
	public String getHighOffsetHours() {
		return highOffsetHours;
	}
	public void setHighOffsetHours(String highOffsetHours) {
		this.highOffsetHours = highOffsetHours;
	}

	public String getHighOffsetMins() {
		return highOffsetMins;
	}
	public void setHighOffsetMins(String highOffsetMins) {
		this.highOffsetMins = highOffsetMins;
	}
	
	public String getHighOffsetSign() {
		return highOffsetSign;
	}
	public void setHighOffsetSign(String highOffsetSign) {
		this.highOffsetSign = highOffsetSign;
	}

	public int getOffsetInMinutes(String hours, String minutes, String sign) {
		int offset = 0;
		if (hasValue(hours)) {
			offset += Integer.parseInt(hours) * 60;
		}
		if (hasValue(minutes)) {
			offset += Integer.parseInt(minutes);
		}
		if (sign.startsWith("-") && !hours.startsWith("-")) {
			offset *= -1;
		} else {
			if (hasValue(minutes) && !hours.startsWith("-")) {
				offset *= -1;
			}
		}
		return offset;
	}
	
	public int getHighOffsetInMinutes() {
		int result = 0;
		if (this.hasHighTideTimeOffset()) {
			result = getOffsetInMinutes(this.highOffsetHours, this.highOffsetMins, this.highOffsetSign);
		} 
		return result;
	}
	
	public String getLowOffsetHours() {
		return lowOffsetHours;
	}
	public void setLowOffsetHours(String lowOffsetHours) {
		this.lowOffsetHours = lowOffsetHours;
	}
	
	public String getLowOffsetMins() {
		return lowOffsetMins;
	}
	public void setLowOffsetMins(String lowOffsetMins) {
		this.lowOffsetMins = lowOffsetMins;
	}
	
	public String getLowOffsetSign() {
		return lowOffsetSign;
	}
	public void setLowOffsetSign(String lowOffsetSign) {
		this.lowOffsetSign = lowOffsetSign;
	}

	public int getLowOffsetInMinutes() {
		int result = 0;
		if (this.hasLowTideTimeOffset()) {
			result = getOffsetInMinutes(this.lowOffsetHours, this.lowOffsetMins, this.lowOffsetSign);
		} 
		return result;
	}

	
	public String getHighOffsetFeet() {
		return highOffsetFeet;
	}
	public void setHighOffsetFeet(String highOffsetFeet) {
		this.highOffsetFeet = highOffsetFeet;
	}
	
	public String getLowOffsetFeet() {
		return lowOffsetFeet;
	}
	public void setLowOffsetFeet(String lowOffsetFeet) {
		this.lowOffsetFeet = lowOffsetFeet;
	}
	
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getNwsOffice() {
		return nwsOffice;
	}
	public void setNwsOffice(String nwsOffice) {
		this.nwsOffice = nwsOffice;
	}
	
	public String getNwsId() {
		return nwsId;
	}
	public void setNwsId(String nwsId) {
		this.nwsId = nwsId;
	}

	public boolean isEnabled() {
		return enabled.equals(1);
	}
	public boolean getEnabled() {
		return isEnabled();
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled ? 1 : 0;
	}
	
	//http://tidesandcurrents.noaa.gov/cgi-bin/get_predictions.cgi?year=2010&stn=7882%20Suva%20Harbor&secstn=Tailevu,%20Viti%20Levu%20Island&thh=+0&thm=0&tlh=-0&tlm=6&hh=+0.8&hl=+1.0&size=small
	public String getUrlParams() {
		String result = "";
		result += "&stn=" + this.stationId;
		if (this.secStationId != null && this.secStationId.length() > 0) {
			result += "&secstn=" + this.secStationId;
		}
		if (this.hasHighTideTimeOffset()) {
			if (hasValue(this.highOffsetHours)) {
				result += "&thh=";
				if (this.highOffsetSign.startsWith("-") && !this.highOffsetHours.startsWith("-")) {
					result += "-";
				} else {
					if (hasValue(this.highOffsetMins) && !this.highOffsetHours.startsWith("-")) {
						result += "%2B";
					}
				}
				result +=	this.highOffsetHours;
			}
			if (hasValue(this.highOffsetMins)) {
				result += "&thm=" + this.highOffsetMins;
			}
		}
		
		if (this.hasLowTideTimeOffset()) {
			if (hasValue(this.lowOffsetHours)) {
				result += "&tlh=";
				if (this.lowOffsetSign.startsWith("-") && !this.lowOffsetHours.startsWith("-")) {
					result += "-";
				} else {
					if (hasValue(this.highOffsetMins) && !this.lowOffsetHours.startsWith("-")) {
						result += "%2B";
					}
				}
				result +=	this.lowOffsetHours;
			}
			if (hasValue(this.lowOffsetMins)) {
				result += "&tlm=" + this.lowOffsetMins;
			}
		}
		
		if (hasValue(this.highOffsetFeet)) {
			if (!this.highOffsetFeet.startsWith("-") && !this.highOffsetFeet.startsWith("+")) {
				result += "&hh=*";
			} else {
				result += "&hh=";
			}
			result += this.highOffsetFeet;
		}
		
		if (hasValue(this.lowOffsetFeet)) {
			if (!this.lowOffsetFeet.startsWith("-") && !this.lowOffsetFeet.startsWith("+")) {
				result += "&hl=*";
			} else {
				result += "&hl=";
			}
			result += this.lowOffsetFeet;
		}
		
		return result.replace(" ", "+");
	}
	
	public String getApiUrlParams() {
		String result = "";
		result += "&Stationid=" + this.nwsId;
		if (this.getHighOffsetInMinutes() != 0) {
			result += "&TimeOffsetHigh=" + this.getHighOffsetInMinutes();
		}
		if (this.getLowOffsetInMinutes() != 0) {
			result += "&TimeOffsetLow=" + this.getLowOffsetInMinutes();
		}
		
		if (hasValue(this.highOffsetFeet)) {
			if (!this.highOffsetFeet.startsWith("-") && !this.highOffsetFeet.startsWith("+")) {
				result += "&HeightOffsetHigh=*";
			} else {
				result += "&HeightOffsetHigh=";
			}
			result += this.highOffsetFeet;
		}
		
		if (hasValue(this.lowOffsetFeet)) {
			if (!this.lowOffsetFeet.startsWith("-") && !this.lowOffsetFeet.startsWith("+")) {
				result += "&HeightOffsetLow=*";
			} else {
				result += "&HeightOffsetLow=";
			}
			result += this.lowOffsetFeet;
		}
		return result.replace(" ", "+");
	}

	
	public String getMarineForecastUrl() {
		//String url = "http://weather.noaa.gov/cgi-bin/fmtbltn.pl?file=forecasts/marine/coastal/"; // this one puts formatting in that we don't want.
		String url = "http://weather.noaa.gov/pub/data/forecasts/marine/coastal/";
		url += this.zone.substring(0,2).toLowerCase(Locale.US) + "/" + this.zone.toLowerCase(Locale.US) + ".txt";
		return url;
	}
	
	public Station(Integer id, String state, String place, String subregion, String stationId, 
			String secStationId, String highOffsetHours, String highOffsetMins,
			String highOffsetSign, String lowOffsetHours, String lowOffsetMins,
			String lowOffsetSign, String highOffsetFeet, String lowOffsetFeet,
			Double latitude,	Double longitude, String zone, String nwsId, Integer enabled) {
		super();
		this.id = id;
		this.state = state;
		this.place = place;
		this.stationId = stationId;
		this.secStationId = secStationId;
		this.subregion = subregion;
		this.latitude = latitude;
		this.longitude = longitude;
		this.highOffsetHours = highOffsetHours;
		this.highOffsetMins = highOffsetMins;
		this.highOffsetSign = highOffsetSign;
		this.lowOffsetHours = lowOffsetHours;
		this.lowOffsetMins = lowOffsetMins;
		this.lowOffsetSign = lowOffsetSign;
		this.highOffsetFeet = highOffsetFeet;
		this.lowOffsetFeet = lowOffsetFeet;
		this.zone = zone;
		this.nwsId = nwsId;
		this.enabled = enabled;
	}
	public int compareTo(Station that) {
		return this.getShortName().compareTo(that.getShortName());
	}
	
	private boolean hasHighTideTimeOffset() {
		return hasValue(this.highOffsetHours) || hasValue(this.highOffsetMins);
	}
	
	private boolean hasLowTideTimeOffset() {
		return hasValue(this.lowOffsetHours) || hasValue(this.lowOffsetMins);
	}
	
	private boolean hasValue(String value) {
		return value != null && value.trim().length() > 0;
	}
}
