package com.cereslogic.tidecast.persist;

import android.os.Parcel;
import android.os.Parcelable;

public class Subregion extends IdObject {
	private String mText;
	private Integer mId;

	public Subregion(String text, Integer id) {
		super();
		this.mText = text;
		this.mId = id;
	}
	
  public void writeToParcel(Parcel out, int flags) {  
		out.writeInt(mId);
		out.writeString(mText);
  }  
	
	private void readFromParcel(Parcel in) {
		mId = in.readInt();
		mText = in.readString();
  }  
  
  public Subregion(Parcel in) {  
    readFromParcel(in);  
   }
  
  public static final Parcelable.Creator<Subregion> CREATOR = new Parcelable.Creator<Subregion>() {  
    public Subregion createFromParcel(Parcel in) {  
    	return new Subregion(in);  
    }  
    public Subregion[] newArray(int size) {  
    	return new Subregion[size];  
    }  
  };  
  
  public int describeContents() {  
    return this.hashCode();  
  } 

	public String getText() {
		return this.mText;
	}
	public void setText(String text) {
		this.mText = text;
	}
	public Integer getId() {
		return this.mId;
	}
	public void setId(Integer id) {
		this.mId = id;
	}	
}
