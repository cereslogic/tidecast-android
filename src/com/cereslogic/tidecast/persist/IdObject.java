package com.cereslogic.tidecast.persist;

import android.os.Parcelable;

public abstract class IdObject implements Parcelable {
	public abstract Integer getId();
	public abstract String getText();
}
