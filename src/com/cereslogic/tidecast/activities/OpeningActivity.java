package com.cereslogic.tidecast.activities;

import java.io.Serializable;
import java.util.List;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.actionbarsherlock.app.SherlockActivity;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.StateOrRegion;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Prefs;

public class OpeningActivity extends SherlockActivity {	
	private Prefs mPrefs = null;
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setTheme(R.style.Theme_Tidecast);
    setContentView(R.layout.opening_layout);
    mPrefs = new Prefs(getBaseContext());
    if (mPrefs.hasSeenDisclaimer() == false) {
    	new AlertDialog.Builder(this)
    		.setMessage(R.string.disclaimer)
    		.setCancelable(false)
    		.setTitle(R.string.terms_of_use)
    		.setPositiveButton(R.string.accept_terms, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
			    	mPrefs.setSeenDisclaimer(true);
					}
				})
    		.show();
    }
  }
  
  @Override
  public void onResume() {
  	super.onResume();
  	// TODO - Figure these out programatically.
  	this.findViewById(R.id.list_option).setBackgroundColor(0xffffffff);
  	this.findViewById(R.id.favorites_option).setBackgroundColor(0xffffffff);
  	this.findViewById(R.id.closest_option).setBackgroundColor(0xffffffff);
  	this.findViewById(R.id.map_option).setBackgroundColor(0xffffffff);
  	this.findViewById(R.id.preferences_option).setBackgroundColor(0xffffffff);
  }
  
  public void onClick(View view) {
  	String activityName = null;
    Intent intent = new Intent(Intent.ACTION_VIEW);
    view.setBackgroundColor(0xa0c0c0e0);
  	switch(view.getId()) {
	  	case R.id.list_option:
	  		activityName = "com.cereslogic.tidecast.activities.MenuListActivity";
	  		List<StateOrRegion> statesOrRegions = Dao.getInstance(getBaseContext()).getStatesOrRegions();
	  		intent.putExtra("listItems", (Serializable)statesOrRegions);
	  		intent.putExtra("title", getString(R.string.choose_region));
	  		break;
	  	case R.id.favorites_option:
	      List<Station> favorites = mPrefs.getFavoriteStations();
	      if (favorites.isEmpty()) {
	      	activityName = "com.cereslogic.tidecast.activities.EmptyFavoritesActivity";
	      } else {
	      	activityName = "com.cereslogic.tidecast.activities.MenuListActivity";
	    		intent.putExtra("listItems", (Serializable)favorites);
	    		intent.putExtra("title", getString(R.string.choose_favorite));
	      }
	  		break;
	  	case R.id.map_option:
	  		activityName = "com.cereslogic.tidecast.activities.MapChooserActivity";
	  		break;
	  	case R.id.closest_option:
	  		activityName = "com.cereslogic.tidecast.activities.LocationatorActivity";
	  		break;
	  	case R.id.preferences_option:
	  		activityName = "com.cereslogic.tidecast.activities.PreferencesActivity";
	  		break;
  	}
    intent.setComponent(new ComponentName("com.cereslogic.tidecast",activityName));
    startActivity(intent);
  }
}