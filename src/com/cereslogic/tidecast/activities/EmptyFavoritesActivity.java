package com.cereslogic.tidecast.activities;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;

public class EmptyFavoritesActivity extends SherlockActivity {
	
	@Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTheme(R.style.Theme_Tidecast);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    setContentView(R.layout.empty_favorites);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
}
