package com.cereslogic.tidecast.activities;

import java.lang.ref.SoftReference;
import java.util.Date;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.Window;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.LocationClient;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Logger;

public class LocationatorActivity extends SherlockActivity {
	
	// Location handler. Gymnastics w/ WeakReferences to prevent memory leaks 
	// (Activities with handlers can't be GC'd because message queue still has a 
	// reference to the handler).
	//
	private final static class HandlerExtension extends Handler {
		private final SoftReference<LocationatorActivity> mLocationatorActivity;
		
		public HandlerExtension(LocationatorActivity locationatorActivity) {
			mLocationatorActivity = new SoftReference<LocationatorActivity>(locationatorActivity);
		}

		public void handleMessage(final Message msg) {
			Logger.d("Got message in Locationator.");
			LocationatorActivity locationatorActivity = mLocationatorActivity.get();
			if (locationatorActivity != null) {
				locationatorActivity.progressDialog.dismiss(); 
				Double lat = msg.getData().getDouble(LocationClient.LATITUDE);
				Double lon = msg.getData().getDouble(LocationClient.LONGITUDE);
				if (lat.equals(LocationClient.INVALID_LAT_LONG) && lon.equals(LocationClient.INVALID_LAT_LONG)) {
					Logger.d("Location is unknown.");
					Toast.makeText(locationatorActivity.getApplicationContext(), R.string.cant_determine_location, Toast.LENGTH_LONG).show();
				} else {
					Logger.d("Location is " + lat + " , " + lon);
					Station closest = Dao.getInstance(locationatorActivity.getBaseContext()).findClosestStation(lon,lat);
					if (closest != null) {
						Logger.d("Closest station is " + closest.getPlace());
						Logger.d("Closest station is " + closest.getShortName());
		        Intent intent = new Intent(Intent.ACTION_VIEW);
		        intent.putExtra("station", (Parcelable)closest);
		        intent.putExtra("day", new Date());
		        intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultListActivity"));
		        locationatorActivity.startActivity(intent);
		        locationatorActivity.finish();
					} else {
						Logger.d("Problem identifying the closest station.");
					}
				}
				mLocationatorActivity.clear();
			}
		}
	}
	private final Handler handler = new HandlerExtension(this);
	
	private LocationManager locationManager;
	private LocationClient lc = null;
	private ProgressDialog progressDialog = null;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setTheme(R.style.Theme_Tidecast);
		this.setContentView(R.layout.tidecast_empty_background);
		this.locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
		if (this.locationManager == null) {
			Toast.makeText(getApplicationContext(), R.string.cant_determine_location, Toast.LENGTH_LONG).show();
		} else {
			this.progressDialog = ProgressDialog.show(this, "Working...", "Determining Current Location...");
			this.progressDialog.setOwnerActivity(this);
			this.lc = new LocationClient();
			this.lc.getLocationAsync(this.locationManager, this.handler);
		}
	}
}
