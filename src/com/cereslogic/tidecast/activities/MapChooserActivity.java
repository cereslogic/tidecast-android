package com.cereslogic.tidecast.activities;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Logger;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

import de.android1.overlaymanager.ManagedOverlay;
import de.android1.overlaymanager.ManagedOverlayGestureDetector;
import de.android1.overlaymanager.ManagedOverlayItem;
import de.android1.overlaymanager.OverlayManager;
import de.android1.overlaymanager.ZoomEvent;
import de.android1.overlaymanager.lazyload.LazyLoadCallback;
import de.android1.overlaymanager.lazyload.LazyLoadException;
                          
// TODO - Make this work w/ Sherlock (there's a plugin available)...
public class MapChooserActivity extends MapActivity {
	private MapView mMapView;
	private MapController mMapController;
	private OverlayManager mOverlayManager;
	private MyLocationOverlay mMyLocationOverlay;
	
	// Extend the ManagedOverlayItem so we can stuff some more information
	// away for use later.
	private class ManagedStationOverlayItem extends ManagedOverlayItem {
		private Integer id;
		public ManagedStationOverlayItem(GeoPoint point, String title, String snippet, Integer id) {
			super(point, title, snippet);
			this.id = id;
		}
		public Integer getId() { return this.id; }
	}
	
	// Handles tapping on the map. Creates an AlertDialog to confirm.
	private ManagedOverlayGestureDetector.OnOverlayGestureListener mOnOverlayGestureListener = new ManagedOverlayGestureDetector.OnOverlayGestureListener() {
		public boolean onDoubleTap(MotionEvent arg0, ManagedOverlay arg1, GeoPoint arg2, ManagedOverlayItem arg3) { return false; }
		public void onLongPress(MotionEvent arg0, ManagedOverlay arg1) { }
		public void onLongPressFinished(MotionEvent arg0, ManagedOverlay arg1, GeoPoint arg2, ManagedOverlayItem arg3) { }
		public boolean onScrolled(MotionEvent arg0, MotionEvent arg1, float arg2,	float arg3, ManagedOverlay arg4) { return false; }
		public boolean onZoom(ZoomEvent arg0, ManagedOverlay arg1) { return false; }
		public boolean onSingleTap(MotionEvent motionEvent, ManagedOverlay managedOverlay, GeoPoint geoPoint, final ManagedOverlayItem managedOverlayItem) {
			if (managedOverlayItem != null) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MapChooserActivity.this);
				builder.setMessage(managedOverlayItem.getSnippet())
			       	 .setTitle(R.string.station_name);
				builder.setPositiveButton(R.string.view_forecast, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
             Intent intent = new Intent(Intent.ACTION_VIEW);
             Station station = Dao.getInstance(getBaseContext()).getStation(((ManagedStationOverlayItem)managedOverlayItem).getId());
             intent.putExtra("station", (Parcelable)station);
             intent.putExtra("day", new Date());
             intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultListActivity"));
             startActivity(intent);
					}
				});
				builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
             Logger.i("Cancel");
					}
				});					
				AlertDialog dialog = builder.create();
				dialog.show();
			}
			return false;
		}
	};
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_station_by_map);
	    
		mMapView = (MapView)findViewById(R.id.mapview);
		mMapView.setBuiltInZoomControls(true);
		
		mMapController = mMapView.getController();
		mMapController.setZoom(13);

		// The MyLocationOverlay centers the map over the current
		// position easily and painlessly.
		mMyLocationOverlay = new MyLocationOverlay(this, mMapView);
		mMyLocationOverlay.enableMyLocation();
		mMyLocationOverlay.runOnFirstFix(new Runnable() {
			public void run() {
				mMapController.animateTo(mMyLocationOverlay.getMyLocation());
				mMapController.setZoom(13);
			}
		});
		mMapView.getOverlays().add(mMyLocationOverlay);
		mOverlayManager = new OverlayManager(getApplication(), mMapView);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		ImageView loaderanim = (ImageView) findViewById(R.id.loader);
		Drawable drawable = this.getResources().getDrawable(R.drawable.mm_20_blue);
		ManagedOverlay managedOverlay = mOverlayManager.createOverlay("lazyloadOverlay",drawable);
		managedOverlay.setOnOverlayGestureListener(mOnOverlayGestureListener);
		managedOverlay.enableLazyLoadAnimation(loaderanim);
		managedOverlay.setLazyLoadCallback(new LazyLoadCallback() {
			public List<ManagedOverlayItem> lazyload(GeoPoint topLeft, GeoPoint bottomRight, ManagedOverlay overlay) throws LazyLoadException {
				return populateOverlayArray(topLeft, bottomRight, overlay.getZoomlevel());
			}
		});
		mOverlayManager.populate();
		managedOverlay.invokeLazyLoad(500);
	}
	 
	@Override
	public void onWindowFocusChanged(boolean b) {
		mOverlayManager.populate();	
	}
	
	// Called by the lazy load callback to return the stuff that's on the screen.
	private List<ManagedOverlayItem> populateOverlayArray(GeoPoint topLeft, GeoPoint bottomRight, int zoomLevel) {
		List<ManagedOverlayItem> items = new LinkedList<ManagedOverlayItem>();
		List<Station> stations = Dao.getStationsInArea(topLeft.getLatitudeE6() / 1000000.0,
				                                                topLeft.getLatitudeE6() / 1000000.0,
				                                                bottomRight.getLongitudeE6() / 1000000.0,
				                                                bottomRight.getLatitudeE6() / 1000000.0);
		for (Station station : stations) {
			GeoPoint point = new GeoPoint((int)(station.getLatitude() * 1000000), (int)(station.getLongitude() * 1000000));
			ManagedStationOverlayItem item = new ManagedStationOverlayItem(point, station.getId().toString(), station.getShortName(), station.getId());
			if (zoomLevel == 0 || zoomLevel == 1 || isVisible(topLeft, bottomRight, point)) {
				items.add(item);
	    }
		}
		return items;
	}
	 
	// Don't really need this anymore now that I have a query (getStationsInArea) doing it?
	private boolean isVisible(GeoPoint topLeft, GeoPoint bottomRight, GeoPoint target) {
		if (topLeft.getLongitudeE6() > bottomRight.getLongitudeE6()) {
			return ((target.getLongitudeE6() > topLeft.getLongitudeE6() || target.getLongitudeE6() < bottomRight.getLongitudeE6())
        		     && target.getLatitudeE6() < topLeft.getLatitudeE6() && target.getLatitudeE6() > bottomRight.getLatitudeE6());
	 	} else {
	 		return (target.getLongitudeE6() > topLeft.getLongitudeE6() && target.getLatitudeE6() < topLeft.getLatitudeE6()
					 && target.getLongitudeE6() < bottomRight.getLongitudeE6() && target.getLatitudeE6() > bottomRight.getLatitudeE6());
	 	}
	}
}
