package com.cereslogic.tidecast.activities;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.utils.Logger;
import com.cereslogic.tidecast.utils.Prefs;

public class PreferencesActivity extends SherlockPreferenceActivity {
	@Override
  protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    setTheme(R.style.Theme_Tidecast);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		addPreferencesFromResource(R.xml.preferences);

		// We have to go through these gymnastics because I stored the preferences
		// in a non-default file and now I have to live with my n00b mistake.
		Preference autoWidgetPref = (Preference) findPreference("com.cereslogic.tidecast.autowidget");
    autoWidgetPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
    	public boolean onPreferenceClick(Preference preference) {
    		Prefs prefs = new Prefs(getBaseContext());
        CheckBoxPreference cb = (CheckBoxPreference) preference;
        Logger.d("Setting autowidget to " + cb.isChecked());
    		prefs.setAutoWidget(cb.isChecked());
    		Toast.makeText(getBaseContext(), getResources().getString(R.string.auto_widget_toast), Toast.LENGTH_LONG).show();
    		return true;
    	}	
    });
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
}
