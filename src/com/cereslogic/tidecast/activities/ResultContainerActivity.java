package com.cereslogic.tidecast.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.fragments.MarineForecastResultFragment;
import com.cereslogic.tidecast.fragments.TideResultFragment;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Prefs;

public class ResultContainerActivity extends SherlockFragmentActivity {
	private Prefs mPrefs;
	private Station mStation;
	
	// Set widget station.
	public void actionButton(View v) {
		mPrefs.setWidgetStation(this.mStation.getId());
		mPrefs.setAutoWidget(false);
		v.setEnabled(false);
		String text = String.format(getResources().getString(R.string.widget_station), mStation.getShortName());
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
	}

	// Toggle favorite.
	public void altActionButton(View v) {
		Button b = (Button)v;
		Integer stationId = mStation.getId();
		Resources res = getResources();
		if (mPrefs.isFavorite(stationId)) {
			mPrefs.removeFavorite(stationId);
			String text = String.format(res.getString(R.string.removed_favorite), mStation.getShortName());
			b.setText(getResources().getString(R.string.add_to_favorites));
			Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
		} else {
			mPrefs.addFavorite(stationId);
			String text = String.format(res.getString(R.string.saved_favorite), mStation.getShortName());
			b.setText(getResources().getString(R.string.remove_favorite));
			Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
		}
	}
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
    setTheme(R.style.Theme_Tidecast);
    super.onCreate(savedInstanceState);

    setContentView(R.layout.result_container);

	  Intent intent = getIntent();
    mStation = (Station)intent.getExtras().getParcelable("station");
    mPrefs = new Prefs(this);
    
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
      
    ActionBar.Tab tideTab = getSupportActionBar().newTab().setText(R.string.tides);
    tideTab.setTabListener(new ResultTabsListener<TideResultFragment>(this, "tideForecast", TideResultFragment.class));
    getSupportActionBar().addTab(tideTab);

    ActionBar.Tab marineForecastTab = getSupportActionBar().newTab().setText(R.string.marine_forecast);
    marineForecastTab.setTabListener(new ResultTabsListener<MarineForecastResultFragment>(this, "marineForecast", MarineForecastResultFragment.class));
    getSupportActionBar().addTab(marineForecastTab);
  }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
  
  public static class ResultTabsListener<T extends Fragment> implements ActionBar.TabListener {
    private Fragment mFragment;
    private final Activity mActivity;
    private final String mTag;
    private final Class<T> mClass;

    /** Constructor used each time a new tab is created.
      * @param activity  The host Activity, used to instantiate the fragment
      * @param tag  The identifier tag for the fragment
      * @param clz  The fragment's Class, used to instantiate the fragment
      */
    public ResultTabsListener(Activity activity, String tag, Class<T> clz) {
        mActivity = activity;
        mTag = tag;
        mClass = clz;
    }

    public void onTabSelected(Tab tab, FragmentTransaction ignoredFt) {
      FragmentManager fragMgr = ((FragmentActivity)mActivity).getSupportFragmentManager();
      FragmentTransaction ft = fragMgr.beginTransaction();

      // Check if the fragment is already initialized
      if (mFragment == null) {
          // If not, instantiate and add it to the activity
          mFragment = Fragment.instantiate(mActivity, mClass.getName());
          ft.add(android.R.id.content, mFragment);
      } else {
          // If it exists, simply attach it in order to show it
          ft.attach(mFragment);
      }
      ft.commit();
    }

    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        if (mFragment != null) {
            // Detach the fragment, because another one is being attached
            ft.detach(mFragment);
        }
    }

    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        // User selected the already selected tab. Usually do nothing.
    }
  }
}