/*
 * Copyright 2010 Ceres Logic LLC.  All Rights Reserved.
 * @author Mike Desjardins
 */
package com.cereslogic.tidecast.activities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.IdObject;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.persist.Subregion;
import com.cereslogic.tidecast.ui.MenuListAdapter;
import com.cereslogic.tidecast.utils.Logger;

public class MenuListActivity extends SherlockListActivity {
	private MenuListAdapter mMenuListAdapter = null;
	private ArrayList<? extends IdObject> mList;
	
	private Comparator<Station> sortByAlpha = new Comparator<Station>() {
		public int compare(Station l, Station r) {
			return l.getPlace().compareTo(r.getPlace());
		}
	};

	private Comparator<Station> sortByLatitude = new Comparator<Station>() {
		public int compare(Station l, Station r) {
			return l.getLatitude().compareTo(r.getLatitude());
		}
	};

	private Comparator<Station> sortByLongitude = new Comparator<Station>() {
		public int compare(Station l, Station r) {
			return l.getLongitude().compareTo(r.getLongitude());
		}
	};
	
	private ActionBar.OnNavigationListener mOnNavigationListener = new ActionBar.OnNavigationListener() {
		@SuppressWarnings("unchecked")
		public boolean onNavigationItemSelected(int itemPosition, long itemId) {
			Logger.i("in onNavigationItemSelected. itemPosition is " + itemPosition + ". itemId is " + itemId + ".");
      switch (itemPosition) {
			case 1:
				Collections.sort(((List<Station>)mList),sortByLatitude);
				break;
			case 2:
				Collections.sort(((List<Station>)mList),sortByLongitude);
				break;
			case 0:
			default:
				Collections.sort(((List<Station>)mList),sortByAlpha);
			}
      MenuListAdapter adapter = (MenuListAdapter)getListAdapter();
      adapter.setChoices(mList);
      adapter.notifyDataSetChanged();
			return false;
		}
	};

	// main "routing" logic
	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      IdObject selected = (IdObject)mMenuListAdapter.getItem(position);
      if (selected.getClass().getSimpleName().equals("StateOrRegion")) {
      	if (Dao.getInstance(getBaseContext()).hasSubregions(selected.getId())) {
      		// Need to select from a subregion next
      		ArrayList<Subregion> subregions = Dao.getInstance(getBaseContext()).getSubregionsForState(selected.getId());
      		intent.putParcelableArrayListExtra("listItems", subregions);
      		intent.putExtra("title", getString(R.string.choose_subregion));
      	} else {
      		// Go straight to the stations associated w/ this state/region.
      		ArrayList<Station> stations = Dao.getInstance(getBaseContext()).getStationsForState(selected.getId(),"name");
      		intent.putParcelableArrayListExtra("listItems", stations);
      		intent.putExtra("title", getString(R.string.choose_station));
      		intent.putExtra("optionsArrayId", R.array.sort_order);
      	}
      	intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.MenuListActivity"));
      } else if (selected.getClass().getSimpleName().equals("Subregion")) {
    		ArrayList<Station> stations = Dao.getInstance(getBaseContext()).getStationsForSubregion(selected.getId(),"name");
    		intent.putParcelableArrayListExtra("listItems", stations);
    		intent.putExtra("listItems", (Serializable)stations);
    		intent.putExtra("title", getString(R.string.choose_station));
    		intent.putExtra("optionsArrayId", R.array.sort_order);
    		intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.MenuListActivity"));
      } else if (selected.getClass().getSimpleName().equals("Station")) {
      	// Go to results from a station.
      	intent.putExtra("station", ((Parcelable)selected));
        intent.putExtra("day", new Date());
        //intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultListActivity"));
        intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultContainerActivity"));
        
        // HACK! We need to reload the favorites list every time,
        // in case the user deletes a station from their favorites.
        // Just quit this Activity if they do that. 
        if (getTitle().equals(getString(R.string.choose_favorite))) {
        	finish();
        }
      }
      startActivity(intent);
    }
	};
	
	@Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    go();
  }
	
	@Override
	public void onResume() {
		super.onResume();
		go();
	}
	
	private void go() {
    setTheme(R.style.Theme_Tidecast);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    setContentView(R.layout.menu_list);
    Intent intent = this.getIntent();
		mList = (intent.getExtras().getParcelableArrayList("listItems"));
		String title = intent.getExtras().getString("title");
		if (title != null && !title.trim().equals("")) {
			this.setTitle(title);
		}
		Integer optionsArrayId = intent.getExtras().getInt("optionsArrayId");
		if (optionsArrayId != null && optionsArrayId != 0) {
			setupOptions(optionsArrayId);
		}
		
    mMenuListAdapter = new MenuListAdapter(this, mList); 
    setListAdapter(mMenuListAdapter);

    ListView lv = getListView();
    lv.setTextFilterEnabled(true);

    lv.setOnItemClickListener(mOnItemClickListener);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
	
	private void setupOptions(int optionsArrayId) {
    Context context = getSupportActionBar().getThemedContext();
    ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(context, optionsArrayId, R.layout.sherlock_spinner_item);
    list.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

    getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
    getSupportActionBar().setListNavigationCallbacks(list, mOnNavigationListener);
	}	
}