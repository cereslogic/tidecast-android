package com.cereslogic.tidecast.activities;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.Window;
import android.widget.TextView;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.persist.Dao;

public class SplashActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		setCopyrightText();
		
		final Handler handler = new Handler() {
			public void handleMessage(final Message msg) {
				finish();
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.OpeningActivity"));
	      startActivity(intent);
			}
		};
				
		new Thread() {
  		public void run() {
  			// Copy over our local stations DB. Takes a couple seconds.
  			Dao.getInstance(getBaseContext());
				handler.sendEmptyMessage(0);
  		}
  	}.start();
	}
	
	private void setCopyrightText() {
		TextView textView = (TextView)findViewById(R.id.copyright);
		if (textView != null) {
			int year = new GregorianCalendar().get(Calendar.YEAR);
			textView.setText(Html.fromHtml("&copy;") + " " + year + " " + getString(R.string.company_name));
		}			
	}
}
