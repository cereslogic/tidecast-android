/*
 * Copyright 2010 Ceres Logic LLC.  All Rights Reserved.
 * @author Mike Desjardins
 */
package com.cereslogic.tidecast.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.SunriseSunsetCalculator;
import com.cereslogic.tidecast.client.Tide;
import com.cereslogic.tidecast.client.TideForecastClient;
import com.cereslogic.tidecast.client.TidecastClientException;
import com.cereslogic.tidecast.data.ResultListActionItem;
import com.cereslogic.tidecast.data.ResultListAlmanacEvent;
import com.cereslogic.tidecast.data.ResultListItem;
import com.cereslogic.tidecast.factories.MoonPhaseEventFactory;
import com.cereslogic.tidecast.factories.SunriseSunsetEventFactory;
import com.cereslogic.tidecast.factories.TideEventFactory;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.ui.AlmanacEventListAdapter;
import com.cereslogic.tidecast.utils.Prefs;
import com.cereslogic.tidecast.utils.TimeZoneOffsetCalculator;

public class ResultListActivity extends SherlockListActivity {
	private Station mStation;
	private Date mDay;
	private ProgressDialog mProgressDialog = null;
	private Prefs mPrefs;
	private final static int DATE_DIALOG_ID = 1;
	
	private DatePickerDialog.OnDateSetListener dateSetListener =
		new DatePickerDialog.OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar cal = new GregorianCalendar();
				cal.set(Calendar.YEAR, year);
				cal.set(Calendar.MONTH, monthOfYear);
				cal.set(Calendar.DATE, dayOfMonth);
		    Intent intent = new Intent(Intent.ACTION_VIEW);
		    intent.putExtra("station", (Parcelable)mStation);
		    intent.putExtra("day", cal.getTime());
		    intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultListActivity"));
		    finish();
		    startActivity(intent);
			}
	};

	// Set widget station.
	public void actionButton(View v) {
		mPrefs.setWidgetStation(this.mStation.getId());
		mPrefs.setAutoWidget(false);
		v.setEnabled(false);
		String text = String.format(getResources().getString(R.string.widget_station), mStation.getShortName());
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
	}

	// Toggle favorite.
	public void altActionButton(View v) {
		Button b = (Button)v;
		Integer stationId = mStation.getId();
		Resources res = getResources();
		if (mPrefs.isFavorite(stationId)) {
			mPrefs.removeFavorite(stationId);
			String text = String.format(res.getString(R.string.removed_favorite), mStation.getShortName());
			b.setText(getResources().getString(R.string.add_to_favorites));
			Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
		} else {
			mPrefs.addFavorite(stationId);
			String text = String.format(res.getString(R.string.saved_favorite), mStation.getShortName());
			b.setText(getResources().getString(R.string.remove_favorite));
			Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    setTheme(R.style.Theme_Tidecast);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.almanac_event_list);
	  Intent intent = this.getIntent();
	  mStation = (Station)intent.getExtras().getParcelable("station");
	  mDay = (Date)intent.getExtras().get("day");
		mPrefs = new Prefs(getBaseContext());
		setTitle(mStation.getPlace());
		
		getListView().setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 0) {
					showDialog(DATE_DIALOG_ID);
				}
			}
		});
		
	  mProgressDialog = ProgressDialog.show(this, "Working...", "Fetching Tide Data");
	  new FetchTidesTask().execute();
	}	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
      	finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
	}
	
	@Override 
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.show_tides, menu);
		return super.onCreateOptionsMenu(menu);
	}

	
	@Override 
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()) {
			case R.id.back_one_day:
				handleBackOrForward(-1);
				return true;
			case R.id.forward_one_day:
				handleBackOrForward(1);
				return true;
			case R.id.choose_day:
				showDialog(DATE_DIALOG_ID);
				return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog result = null;
		switch (id) {
			case DATE_DIALOG_ID:
				Calendar cal = new GregorianCalendar();
				cal.setTime(mDay);
				result = new DatePickerDialog(this, dateSetListener, 
						cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
		}
	  return result;
	}
	
	private class FetchTidesTask extends AsyncTask</*Params*/ Void, /* Progress */ Void, /* Result */ List<Tide>> {
    protected List<Tide> doInBackground(Void... nil) {
    	TideForecastClient tideForecastClient = new TideForecastClient();
    	List<Tide> result = new ArrayList<Tide>();
  	  try {
				result = tideForecastClient.getTides(mStation,mDay,mDay);
			} catch (TidecastClientException e) {
				runOnUiThread(new Runnable() {
				  public void run() {
				  	Toast.makeText(getApplicationContext(), R.string.error_contacting_noaa, Toast.LENGTH_SHORT).show();
				  }
				});
				e.printStackTrace();
			}
			return result;
    }
    
    protected void onPostExecute(List<Tide> tides) {
    	if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			Resources resources = getBaseContext().getResources();
			List<ResultListItem> items = new ArrayList<ResultListItem>();
			items.add(createAlamanacEventForDate());
			items.addAll(TideEventFactory.makeTides(tides, resources));
			int offset = TimeZoneOffsetCalculator.getTimeZoneOffset(mStation.getPlace(), mStation.getState(), mDay);
			SunriseSunsetCalculator ss = new SunriseSunsetCalculator(mStation.getLatitude(), mStation.getLongitude(), mDay, offset);				
			ResultListAlmanacEvent sunrise = SunriseSunsetEventFactory.makeSunrise(ss, resources); 
			if (sunrise != null) {
				items.add(sunrise);
			}
			ResultListAlmanacEvent sunset = SunriseSunsetEventFactory.makeSunset(ss, resources);
			if (sunset != null) {
				items.add(sunset);
			}				
			ResultListAlmanacEvent moon = MoonPhaseEventFactory.makeMoonPhase(ResultListActivity.this.mDay, resources);
			if (moon != null) {
				items.add(moon);
			}
			
			String altActionButtonText = mPrefs.isFavorite(mStation.getId()) ? 
					resources.getString(R.string.remove_favorite) : resources.getString(R.string.add_to_favorites);
			ResultListActionItem ai = new ResultListActionItem("Show in Widget",altActionButtonText);
			ai.setActionEnabled(!mPrefs.isWidgetStation(mStation.getId()));
			items.add(ai);
      setListAdapter(new AlmanacEventListAdapter(ResultListActivity.this, items));
    }
	}
	
	private void handleBackOrForward(int days) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(this.mDay);
		cal.add(Calendar.DATE, days);
    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.putExtra("station", (Parcelable)mStation);
    intent.putExtra("day", cal.getTime());
    intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultListActivity"));
    finish();
    startActivity(intent);
	} 	
		
	private ResultListAlmanacEvent createAlamanacEventForDate() {
		return new ResultListAlmanacEvent(
				              getResources().getString(R.string.tap_to_change), 
				              new SimpleDateFormat("EEEE, MMMM d", Locale.ENGLISH).format(mDay),
				              BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.calendar),
				              true);
	}
}
