package com.cereslogic.tidecast.utils;

import android.util.Log;

public class Logger {
	private final static String tag = "Tidecast";
	
	public static void d(String m) {
		Log.d(tag,m);
	}
	public static void d(String m, Throwable t) {
		Log.d(tag,m,t);
	}

	public static void e(String m) {
		Log.e(tag,m);
	}
	public static void e(String m, Throwable t) {
		Log.e(tag,m,t);
	}

	public static void i(String m) {
		Log.i(tag,m);
	}
	public static void i(String m, Throwable t) {
		Log.i(tag,m,t);
	}
	
	public static void v(String m) {
		Log.v(tag,m);
	}
	public static void v(String m, Throwable t) {
		Log.v(tag,m,t);
	}
	
	public static void w(String m) {
		Log.w(tag,m);
	}
	public static void w(String m, Throwable t) {
		Log.w(tag,m,t);
	}
}
