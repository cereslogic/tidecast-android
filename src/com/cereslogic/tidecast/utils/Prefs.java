package com.cereslogic.tidecast.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.cereslogic.tidecast.TideWidgetProvider;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.Station;

public class Prefs {
	private SharedPreferences prefs = null;
	private Editor editor = null;
	private Context context = null;
	private final static int MIN_LEGACY_ID = 1611000;
	private final static String DEFAULT_ID = "88"; // Portland Head Light. PORTLAND REPRESENT!
	
	public Prefs(Context context) {
		this.prefs = context.getSharedPreferences("PREFS_PRIVATE", Context.MODE_PRIVATE);
		this.context = context;
		this.editor = this.prefs.edit();
		upgradeFavorites();
	}
	
	public String getValue(String key, String defaultValue) {
		String result = defaultValue;
		if (this.prefs != null) {
			result = this.prefs.getString(key, defaultValue);
		}
		return result;
	}
	
	public void setValue(String key, String value) {
		if (this.editor != null) {
			editor.putString(key, value);
		}
	}
	
	public void save() {
		if (this.editor != null) {
			this.editor.commit();
		}
	}
	
	public boolean isFavorite(String stationId) {
		return isFavorite(Integer.parseInt(stationId));
	}
	
	public boolean isFavorite(Integer stationId) {
		boolean result = false;
		if (stationId > Prefs.MIN_LEGACY_ID) {
			Station s = Dao.getInstance(this.context).getStationByNwsId(stationId.toString());
			if (s != null) {
				result = getFavorites().contains(s.getId());;
			}
		} else {
			result = getFavorites().contains(stationId); 
		}
		return result; 
	}
	
	public ArrayList<Integer> getFavorites() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		List<String> ids = getFavoritesAsStrings();
		for (String id : ids) {
			if (id.length() > 0) {
				result.add(Integer.parseInt(id));
			}
		}
		return result;
	}
	
	public ArrayList<String> getFavoritesAsStrings() {
		ArrayList<String> result = new ArrayList<String>();
		result.addAll(Arrays.asList(this.getValue("com.cereslogic.tidecast.favorites", "").split(",")));
		return result;
	}

	public List<Station> getFavoriteStations() {
		List<Station> result = new ArrayList<Station>();
		List<Integer> stationIds = getFavorites();
		for (Integer stationId : stationIds) {
			Station station = Dao.getInstance(this.context).getStation(stationId);
			if (station != null) {
				result.add(station);
			} else {
				Logger.e("Couldn't find a station with the ID " + stationId);
			}
		}
		Collections.sort(result);
		return result;
	}
	
	public void addFavorite(Integer newFavorite) {
		if (!isFavorite(newFavorite)) {
			List<String> favs = getFavoritesAsStrings();
			favs.add(newFavorite.toString());
			String result = StringUtils.join(favs, ",");
			this.setValue("com.cereslogic.tidecast.favorites", result);
			this.save();
		}
	}

	public void upgradeFavorites() {		
		String old = this.getValue("com.cereslogic.tidecast.favoriteList", "");
		if (old != null && old.length() > 0) {
			Logger.d("Upgrading favorites.");	
			List<String> legacies = Arrays.asList(old.split(","));
			for (String legacy : legacies) {
				if (legacy.length() > 0) {
					Station s = Dao.getInstance(this.context).getStationByNwsId(legacy);
					if (s != null) {
						addFavorite(s.getId());
					}
				}
			}
			this.setValue("com.cereslogic.tidecast.favoriteList", "");
			this.save();
			Logger.d("Done upgrading favorites.");
		}
	}
	
	public void removeFavorite(String unFavorite) {
		removeFavorite(Integer.parseInt(unFavorite.toString()));
	}
	
	public void removeFavorite(Integer unFavorite) {
		Logger.d("Unfavoriting " + unFavorite);
		if (isFavorite(unFavorite)) {
			ArrayList<String> favs = getFavoritesAsStrings();
			favs.remove(unFavorite.toString());
			String result = StringUtils.join(favs, ",");
			this.setValue("com.cereslogic.tidecast.favorites", result);
			this.save();
		} else {
			Logger.d("Attempted to unfavorite a non favorite??? ID was " + unFavorite);
		}
	}
	
	public boolean hasSeenDisclaimer() {
		return getValue("com.cereslogic.tidecast.seendisclaimer", "false").equals("true");
	}
	public void setSeenDisclaimer(boolean value) {
		String setTo = (value == true ? "true" : "false");
		this.setValue("com.cereslogic.tidecast.seendisclaimer", setTo);
		this.save();
	}
	
	public boolean isAutoWidget() {
		return getValue("com.cereslogic.tidecast.autowidget", "true").equals("true");
	}
	public void setAutoWidget(boolean value) {
		String setTo = (value == true ? "true" : "false");
		this.setValue("com.cereslogic.tidecast.autowidget", setTo);
		this.save();
		this.updateWidgetSoon();
	}
	
	public Integer getWidgetStation() {
		return Integer.parseInt(getValue("com.cereslogic.tidecast.widgetstation", Prefs.DEFAULT_ID));
	}
	public boolean isWidgetStation(int id) {
		return getWidgetStation().equals(id) && !isAutoWidget();
	}
	public void setWidgetStation(Integer id) {
		this.setValue("com.cereslogic.tidecast.widgetstation", id.toString());
		this.save();
		this.updateWidgetSoon();
	}

	// Changing some of these preferences should force a widget update. 
	private void updateWidgetSoon() {
		Intent intent = new Intent();
		intent.setAction("com.cereslogic.tidecast.intent.forcedUpdate");
		intent.setComponent(new ComponentName(context,TideWidgetProvider.class));
		context.sendBroadcast(intent);
	}
}
