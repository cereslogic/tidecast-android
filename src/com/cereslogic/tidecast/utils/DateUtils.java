package com.cereslogic.tidecast.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {
	public static Date roundBackToMidnight(Date date) {
		GregorianCalendar result = new GregorianCalendar();
		result.setTime(date);
		result.set(Calendar.HOUR, 0);
		result.set(Calendar.MINUTE, 0);
		result.set(Calendar.SECOND, 0);
		result.set(Calendar.MILLISECOND, 0);
		result.set(Calendar.AM_PM, Calendar.AM);
		return result.getTime();
	}
	
	// Does a *whole day* between algorithm, *inclusive*.  E.g.,
	// 8/16/2010 at 14:16 is, according to this algorithm, between
	// anytime on 8/16/2010 and anytime on 8/16/2010.
	//
	public static boolean between(Date tested, Date begin, Date end) {
		boolean result = false;
		if (tested != null && begin != null && end != null) {
			Date absBegin = roundBackToMidnight(begin);
			Date absEnd = roundBackToMidnight(end);
			Date absTest = roundBackToMidnight(tested);
			result = (absTest.after(absBegin) || absTest.equals(absBegin)) &&	(absTest.before(absEnd) || absTest.equals(absEnd));
		}
		return result;
	}

	public static Date nextDay(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}
	
	// time is in HH:MM format
	@SuppressWarnings("deprecation")
	public static Date setTime(Date day, String time) {
		Date result = new Date(day.getTime());
		Integer hours = Integer.parseInt(time.substring(0,time.indexOf(':')));
		Integer mins = Integer.parseInt(time.substring(time.indexOf(':') + 1));
		result.setHours(hours);
		result.setMinutes(mins);
		return result;
	}
}
