package com.cereslogic.tidecast.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import com.cereslogic.tidecast.persist.Station;

// The SunriseSunsetCalculator needs to know the difference between the current
// timezone, and the timezone for the station we're calculating for.
//
public class TimeZoneOffsetCalculator {
	private static Map<String,String> stateTimeZones;
	static {
		stateTimeZones = new HashMap<String,String>();
		stateTimeZones.put("HI","Pacific/Honolulu");
		stateTimeZones.put("PC","Pacific/Midway");
		stateTimeZones.put("BM","Atlantic/Bermuda");
		stateTimeZones.put("BS","America/New_York");
		stateTimeZones.put("MX","America/Los_Angeles");
		stateTimeZones.put("ME","America/New_York");
		stateTimeZones.put("NH","America/New_York");
		stateTimeZones.put("MA","America/New_York");
		stateTimeZones.put("RI","America/New_York");
		stateTimeZones.put("CT","America/New_York");
		stateTimeZones.put("NY","America/New_York");
		stateTimeZones.put("NJ","America/New_York");
		stateTimeZones.put("PA","America/New_York");
		stateTimeZones.put("DE","America/New_York");
		stateTimeZones.put("MD","America/New_York");
		stateTimeZones.put("DC","America/New_York");
		stateTimeZones.put("VA","America/New_York");	
		stateTimeZones.put("NC","America/New_York");
		stateTimeZones.put("SC","America/New_York");
		stateTimeZones.put("GA","America/New_York");
		stateTimeZones.put("FL","America/New_York");
		stateTimeZones.put("AL","America/Chicago");
		stateTimeZones.put("MS","America/Chicago");
		stateTimeZones.put("LA","America/Chicago");
		stateTimeZones.put("TX","America/Chicago");
		stateTimeZones.put("CA","America/Los_Angeles");
		stateTimeZones.put("OR","America/Los_Angeles");
		stateTimeZones.put("WA","America/Los_Angeles");
		stateTimeZones.put("AK","America/Anchorage");
		stateTimeZones.put("VI","America/St_Thomas");
		stateTimeZones.put("PR","America/Puerto_Rico");
		stateTimeZones.put("DO","America/Santo_Domingo");
		stateTimeZones.put("HT","America/Port-au-Prince");
		stateTimeZones.put("MX","America/Tijuana");
		stateTimeZones.put("JM","America/Jamaica");
		stateTimeZones.put("CU","America/Havana");
		stateTimeZones.put("PN","America/Panama");
	}

	// Date passed in is GMT. Returns local time.
	public static Date correctFromGmt(Station station, Date date) {
		TimeZone tz;
		if (station.getState().equals("Alaska") && (station.getPlace().equals("Adak Island") || station.getPlace().equals("Atka"))) {
			tz = TimeZone.getTimeZone("America/Adak");
		} else {
			tz = TimeZone.getTimeZone(stateTimeZones.get(station.getState()));
		}

		Date result = new Date( date.getTime() + tz.getRawOffset() );

		// if we are now in DST, back off by the delta.  Note that we are checking the GMT date, this is the KEY.
	  if ( tz.inDaylightTime(result)) {
	  	Date dstDate = new Date(result.getTime() + tz.getDSTSavings());

	  	// check to make sure we have not crossed back into standard time
	    // this happens when we are on the cusp of DST (7pm the day before the change for PDT)
	    if ( tz.inDaylightTime( dstDate )) {
	    	result = dstDate;
	    }
	  }
	  return result;
	}
	
  // Date passed in is local time. Returns GMT.
	public static Date correctToGmt(Station station, Date date) {
		TimeZone tz;
		if (station.getState().equals("Alaska") && (station.getPlace().equals("Adak Island") || station.getPlace().equals("Atka"))) {
			tz = TimeZone.getTimeZone("America/Adak");
		} else {
			tz = TimeZone.getTimeZone(stateTimeZones.get(station.getState()));
		}

		Date result = new Date( date.getTime() - tz.getRawOffset() );

		// if we are now in DST, back off by the delta.  Note that we are checking the GMT date, this is the KEY.
	  if ( tz.inDaylightTime(result)) {
	  	Date dstDate = new Date(result.getTime() + tz.getDSTSavings());

	  	// check to make sure we have not crossed back into standard time
	    // this happens when we are on the cusp of DST (7pm the day before the change for PDT)
	    if ( tz.inDaylightTime( dstDate )) {
	    	result = dstDate;
	    }
	  }
	  return result;
	}
	
	public static int getTimeZoneOffset(String place, String state, Date when) {
		TimeZone tzHere = TimeZone.getDefault();
		int offsetHere = tzHere.getOffset(when.getTime());
		
		// Special handling for the two stations in the Aleutian timezone.
		TimeZone tzThere;
		if (state.equals("Alaska") && (place.equals("Adak Island") || place.equals("Atka"))) {
			tzThere = TimeZone.getTimeZone("America/Adak");
		} else {
			tzThere = TimeZone.getTimeZone(stateTimeZones.get(state));
		}
		  
		int offsetThere = tzThere.getOffset(when.getTime());
		
		return (offsetThere - offsetHere)/1000/60/60;
	}
}
