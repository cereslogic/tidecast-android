package com.cereslogic.tidecast.factories;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.R.drawable;
import com.cereslogic.tidecast.data.ResultListAlmanacEvent;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

//
// Found this here:
// http://www.voidware.com/moon_phase.htm
//
public class MoonPhaseEventFactory {
	private static GregorianCalendar cal = new GregorianCalendar();
	
	private static int FILE[] = {
		R.drawable.moon_new,
		R.drawable.moon_first_crescent,
		R.drawable.moon_first_quarter,
		R.drawable.moon_waxing_gibbous,
		R.drawable.moon_full,
		R.drawable.moon_waning_gibbous,
		R.drawable.moon_last_quarter,
		R.drawable.moon_last_crescent
	};
	
	private static String LABEL[] = {
		"New", "Crescent", "First Quarter", "Waxing Gibbous", "Full", "Waning Gibbous", "Last Quarter", "Crescent"
	};
	
	public static ResultListAlmanacEvent makeMoonPhase(Date when, Resources resources) {
		int phase = getMoonPhase(when);
		Bitmap b = BitmapFactory.decodeResource(resources, FILE[phase]);
		return new ResultListAlmanacEvent("Moon Phase", LABEL[phase], b, false);
	}
	
	private static int getMoonPhase(Date when) {
    /*
    calculates the moon phase (0-7), accurate to 1 segment.
    0 = > new moon.
    4 => full moon.
    */
		cal.setTime(when);
		int y = cal.get(Calendar.YEAR);
		int m = cal.get(Calendar.MONTH);
		int d = cal.get(Calendar.DAY_OF_MONTH);
		int c,e;
		double jd;
		int b;

		if (m < 3) {
      y--;
      m += 12;
		}
		++m;
		c = (int)(365.25*y);
		e = (int)(30.6*m);
		jd = c+e+d-694039.09;  /* jd is total days elapsed */
		jd /= 29.53;           /* divide by the moon cycle (29.53 days) */
		b = (int)jd;		   /* int(jd) -> b, take integer part of jd */
		jd -= b;		   /* subtract integer part to leave fractional part of original jd */
		b = (int)(jd*8 + 0.5);	   /* scale fraction from 0-8 and round by adding 0.5 */
		b = b & 7;		   /* 0 and 8 are the same so turn 8 into 0 */
		return b;
	}
}
