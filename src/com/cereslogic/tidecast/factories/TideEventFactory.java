package com.cereslogic.tidecast.factories;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.Tide;
import com.cereslogic.tidecast.data.ResultListAlmanacEvent;

public class TideEventFactory {
	public static List<ResultListAlmanacEvent> makeTides(List<Tide> tides,Resources resources) {
		List<ResultListAlmanacEvent> events = new ArrayList<ResultListAlmanacEvent>();
		if (tides != null) {
			for (Tide tide : tides) {
				events.add(makeAlmanacEvent(tide,resources));
			}
		}
		return events;
	}
	
	private static ResultListAlmanacEvent makeAlmanacEvent(Tide tide,Resources resources) {
		String label = tide.isLowTide() ? resources.getString(R.string.low_tide) : resources.getString(R.string.high_tide);
		String value = new SimpleDateFormat("h:mma",Locale.US).format(tide.getWhen()).toLowerCase(Locale.US) + ",  " + tide.getFeet() + "'";
		Bitmap bitmap = tide.isHighTide() ?  
				BitmapFactory.decodeResource(resources, R.drawable.hi_tide) : 
				BitmapFactory.decodeResource(resources, R.drawable.lo_tide);
		return new ResultListAlmanacEvent(label,value,bitmap,false);
	}
}
