package com.cereslogic.tidecast.factories;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.SunriseSunsetCalculator;
import com.cereslogic.tidecast.data.ResultListAlmanacEvent;

public class SunriseSunsetEventFactory {
	public static ResultListAlmanacEvent makeSunrise(SunriseSunsetCalculator ss, Resources resources) {
		ResultListAlmanacEvent result = null;
		Date sunrise = ss.getSunrise();
		if (sunrise != null) {
			result = new ResultListAlmanacEvent(
										resources.getString(R.string.sunrise),
										new SimpleDateFormat("h:mma",Locale.US).format(sunrise).toLowerCase(Locale.US),
										BitmapFactory.decodeResource(resources, R.drawable.sunrise),
										false);
		}
		return result;
	}
	
	public static ResultListAlmanacEvent makeSunset(SunriseSunsetCalculator ss, Resources resources) {
		ResultListAlmanacEvent result = null;
		Date sunset = ss.getSunset();
		if (sunset != null) {
			result = new ResultListAlmanacEvent(
										resources.getString(R.string.sunset),
										new SimpleDateFormat("h:mma",Locale.US).format(sunset).toLowerCase(Locale.US),
										BitmapFactory.decodeResource(resources, R.drawable.sunset),
										false); 
		}
		return result; 
	}
}
