/*
 * Copyright 2010 Ceres Logic LLC.  All Rights Reserved.
 * @author Mike Desjardins
 */
package com.cereslogic.tidecast.client;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tide implements Serializable {
	private static final long serialVersionUID = -8498952652112257934L;
	private Date when;
	private Float feet;
	private boolean isLowTide;
	
	public Date getWhen() {
		return when;
	}
	public void setWhen(Date when) {
		this.when = when;
	}
	
	public Float getFeet() {
		return feet;
	}
	public void setFeet(Float feet) {
		this.feet = feet;
	}
	
	public boolean isLowTide() {
		return isLowTide;
	}
	public void setLowTide(boolean isLowTide) {
		this.isLowTide = isLowTide;
	}
	public boolean isHighTide() {
		return !isLowTide;
	}
	public void setHighTide(boolean isHighTide) {
		this.isLowTide = !isHighTide;
	}
	
	public Tide(Date when, Float feet, boolean isLowTide) {
		super();
		this.when = when;
		this.feet = feet;
		this.isLowTide = isLowTide;
	}
 	
	public String toString() {
		return new SimpleDateFormat("MM/dd/yyyy HH:mm").format(when) + ", isLowTide: " + isLowTide + ",  " + feet + "'";
	}
}
