package com.cereslogic.tidecast.client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.DateIterator;
import com.cereslogic.tidecast.utils.DateUtils;
import com.cereslogic.tidecast.utils.Logger;
import com.cereslogic.tidecast.utils.StringUtils;

public class TideForecastClient {

	private final static String URL_BASE = "http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT&timelength=daily&dataUnits=1&timeUnits=2&pageview=dayly&print_download=true";
	//private final static String ALT_URL_BASE_2 = "http://140.90.121.109/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT&timelength=daily&dataUnits=1&timeUnits=2&pageview=dayly&print_download=true";
	private final static String ALT_URL_BASE = "http://140.90.78.215/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT&timelength=daily&dataUnits=1&timeUnits=2&pageview=dayly&print_download=true";

	private final static int CONNECTION_TIMEOUT_MILLISECONDS = 10000;
	private final static int SOCKET_TIMEOUT_MILLISECONDS = 10000;
	private final static int PREFETCH_BEGIN_DAYS = 2;
	private final static int PREFETCH_END_DAYS = 14;
	private final static int RETRY_COUNT = 3;

	// All the tides on a given date at a given station.
	private static Map<String, Map<Date,ArrayList<Tide>>> cache = new HashMap<String, Map<Date,ArrayList<Tide>>>(); 

	public List<Tide> getTides(final Station station, final Date begin, final Date end) throws TidecastClientException {
		List<Tide> result = new ArrayList<Tide>();
		List<Tide> cached = getCache(station,begin,end); 
		Logger.d("getTides... begin " + new SimpleDateFormat("yyyyMMdd", Locale.US).format(begin) + ", end " + new SimpleDateFormat("yyyyMMdd", Locale.US).format(end));
		if (cached != null) {
			Logger.d("Results served from cache.");
			return cached; 
		} else {
			String url = null;
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT_MILLISECONDS);
			HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT_MILLISECONDS);
			HttpResponse httpResponse;
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
			httpClient.setHttpRequestRetryHandler(new ExtendedHttpRequestRetryHandler(RETRY_COUNT,true));

			try {
				url = URL_BASE + station.getApiUrlParams() + "&year=" + new SimpleDateFormat("yyyy", Locale.US).format(begin) + "&bdate=" + new SimpleDateFormat("yyyy", Locale.US).format(begin) + "0101";
				HttpGet httpGet = new HttpGet(url);
				Logger.d("Executing HTTP call to " + url);
				httpResponse = httpClient.execute(httpGet);
			} catch (UnknownHostException e) {
				// Sometimes I've noticed the app having trouble resolving the address
				// of the NOAA site.  I have no idea why this happens, but here was
				// the last known IP address that we can try in case that works.
				url = ALT_URL_BASE + station.getApiUrlParams() + "&year=" + new SimpleDateFormat("yyyy", Locale.US).format(begin);
				HttpGet httpGet = new HttpGet(url);
				Logger.d("Executing HTTP call to " + url);
				try {
					httpResponse = httpClient.execute(httpGet);
				} catch (Exception e2) {
					Logger.e("Exception",e2);
					throw new TidecastClientException(e2);
				}
			} catch (Exception e) {
				Logger.e("Exception",e);
				throw new TidecastClientException(e);
			}

			if (httpResponse != null) {
				HttpEntity entity = httpResponse.getEntity(); 
				String body = null;
				try {
					body = StringUtils.inputStreamToString(entity.getContent());
					GregorianCalendar prefetchBegin = new GregorianCalendar();
					prefetchBegin.setTime(begin);
					prefetchBegin.add(Calendar.DATE, -1 * PREFETCH_END_DAYS);

					GregorianCalendar prefetchEnd = new GregorianCalendar();
					prefetchEnd.setTime(end);
					prefetchEnd.add(Calendar.DATE, PREFETCH_BEGIN_DAYS);

					List<Tide> tides = TideParser.parseTides(station,body,prefetchBegin.getTime(),prefetchEnd.getTime());
					putCache(station,tides);

					// We may have prefetched extra dates in the request, we only
					// want to send back what the client asked for.
					for (Tide tide : tides) {
						if (DateUtils.between(tide.getWhen(), DateUtils.roundBackToMidnight(begin), DateUtils.roundBackToMidnight(end))) {
							result.add(tide);
						}
					}

				} catch (IOException e) { 
					Log.e("Tidecast","Caught an error contacting NOAA.",e);
					throw new TidecastClientException(e);
				}	
			}
			return result;		
		}		
	}

	private void putCache(Station station,List<Tide> tides) {
		Map<String, Map<Date,ArrayList<Tide>>> addToCache = new HashMap<String, Map<Date,ArrayList<Tide>>>(); 
		for (Tide tide : tides) {
			Map<Date,ArrayList<Tide>> atThisStation = addToCache.get(station.getStationId());
			if (atThisStation == null) {
				atThisStation = new HashMap<Date,ArrayList<Tide>>();
			}
			Date day = DateUtils.roundBackToMidnight(tide.getWhen());
			ArrayList<Tide> onThisDate = atThisStation.get(day);
			if (onThisDate == null) {
				onThisDate = new ArrayList<Tide>(); 
			}
			onThisDate.add(tide);
			atThisStation.put(day, onThisDate);
			addToCache.put(station.getStationId(), atThisStation);
		}
		TideForecastClient.cache.putAll(addToCache);
	}

	private List<Tide> getCache(Station station, Date begin, Date end) {
		List<Tide> result = new ArrayList<Tide>();
		begin = DateUtils.roundBackToMidnight(begin);
		end = DateUtils.roundBackToMidnight(end);
		Iterator<Date> days = new DateIterator(begin,end);
		while(days.hasNext()) {
			Date day = days.next();
			Map<Date,ArrayList<Tide>> atThisStation = TideForecastClient.cache.get(station.getStationId());
			if (atThisStation == null) {
				Logger.d("Cache miss for " + station.getStationId());
				return null;
			} else {
				List<Tide> onThisDate = atThisStation.get(day);
				if (onThisDate == null) {
					Logger.d("Cache miss for " + station.getStationId() + " " + new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(day));
					return null;
				} else {
					Logger.d("Cache hit for " + station.getStationId() + " " + new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(day));
					result.addAll(onThisDate);
				}
			}
		}
		return result;
	}

	public void clearCache() {
		TideForecastClient.cache.clear();
	}
}
