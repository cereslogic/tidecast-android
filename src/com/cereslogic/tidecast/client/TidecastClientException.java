package com.cereslogic.tidecast.client;

public class TidecastClientException extends Exception {
	private static final long serialVersionUID = -8898479854544139749L;
	private Exception mWrappedException;
	
	public TidecastClientException(Exception wrappedException) {
		super();
		mWrappedException = wrappedException;
	}
	
	public Exception getWrappedException() {
		return mWrappedException;
	}
}
