package com.cereslogic.tidecast.client;

import java.io.IOException;

import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;

import com.cereslogic.tidecast.utils.Logger;

public class ExtendedHttpRequestRetryHandler extends DefaultHttpRequestRetryHandler {
	public ExtendedHttpRequestRetryHandler() {
		this(3,false);
	}
	
	public ExtendedHttpRequestRetryHandler(int retryCount, boolean requestSentRetryEnabled) {
		super(retryCount,requestSentRetryEnabled);
	}

	@Override
	public boolean retryRequest(final IOException exception, int executionCount, final HttpContext context) {
		String message = exception == null ? "(no exception)" : exception.getMessage();
		Logger.d("In retryRequest, got a " + message + ". Retry count is " + exception +".");
		return super.retryRequest(exception, executionCount, context) || 
			(executionCount <= this.getRetryCount() && exception instanceof java.net.SocketTimeoutException);
	}
}
