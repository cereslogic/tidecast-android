package com.cereslogic.tidecast.client;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Logger;
import com.cereslogic.tidecast.utils.StringUtils;

public class MarineForecastClient {

	private final static int CONNECTION_TIMEOUT_MILLISECONDS = 10000;
	private final static int SOCKET_TIMEOUT_MILLISECONDS = 10000;
	private final static int RETRY_COUNT = 3;

	public MarineForecast getForecast(final Station station) throws TidecastClientException {
		MarineForecast result = new MarineForecast();
		
		String url = null;
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT_MILLISECONDS);
		HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT_MILLISECONDS);
		HttpResponse httpResponse;
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
		httpClient.setHttpRequestRetryHandler(new ExtendedHttpRequestRetryHandler(RETRY_COUNT,true));

		try {
			HttpGet httpGet = new HttpGet(station.getMarineForecastUrl());
			Logger.d("Executing HTTP call to " + url);
			httpResponse = httpClient.execute(httpGet);
		} catch (Exception e) {
			Logger.e("Exception",e);
			throw new TidecastClientException(e);
		}

		if (httpResponse != null) {
			HttpEntity entity = httpResponse.getEntity(); 
			String body = null;
			try {
				body = StringUtils.inputStreamToString(entity.getContent());
				result = MarineForecastParser.parseMarineForecast(body);
			} catch (IOException e) { 
				Log.e("Tidecast","Caught an error contacting NOAA.",e);
				throw new TidecastClientException(e);
			}	
		}
		return result;		
	}		
}
