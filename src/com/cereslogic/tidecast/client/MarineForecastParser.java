package com.cereslogic.tidecast.client;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.cereslogic.tidecast.utils.Logger;

public class MarineForecastParser {
	private enum State  {
		BEGIN, SOURCE, FORECAST_DATE, ZONE_DESCRIPTION, ALERT, PRE_FORECAST, FORECAST, END
	};
	
	public static MarineForecast parseMarineForecast(String text) {
		Logger.d("///// RAW TEXT //////////////////////////////////////");
		Logger.d(text);
		Logger.d("/////////////////////////////////////////////////////");
		MarineForecast result = new MarineForecast();
		String[] lines = text.split("\n");
		//1044 AM CDT FRI JUN 3 2011
		SimpleDateFormat sdf = new SimpleDateFormat("hhmm a zzz EEE MMM d yyyy", Locale.US);
		String zoneDescription = "";
		List<MarineDailyForecast> marineDailyForecasts = new ArrayList<MarineDailyForecast>();
		State state = State.BEGIN;
		String forecastAccumulator = "";
		String alertAccumulator = "";
		String forecastDay = "";
		String dateString = "";
		boolean seenForecast = false;
		for (int i=0; i<lines.length; i++) {
			String line = lines[i];
			try {
				//Logger.d("Current state is " + state);
				//Logger.d("forecastAccumulator = " + forecastAccumulator);
				switch (state) {
					case BEGIN:
						if (i == 3) {
							state = State.SOURCE;
						}
						break;
					case SOURCE:
						result.setSource(line);
						state = State.FORECAST_DATE;
						break;
					case FORECAST_DATE:
						try {
							dateString = line;
							Date forecastDate = sdf.parse(dateString);
							result.setForecastDate(forecastDate);
						} catch (ParseException e) {
							Logger.d("Unable to parse date: " + line);
						}
						state = State.ZONE_DESCRIPTION;
						break;
					case ZONE_DESCRIPTION:
						if (line.trim().equals("")) {
							state = State.PRE_FORECAST;
						} else {
							zoneDescription += line.trim() + " ";
						}
						break;
					case PRE_FORECAST:
						if (line.equals(dateString)) {
							state = State.FORECAST;
							break;
						}
					case FORECAST:
						if (!seenForecast) {
							if (line.startsWith("...") && !seenForecast) {
								alertAccumulator += line;
							}
							if (line.endsWith("...")) {
								result.setAlert(alertAccumulator.replaceAll("\\.\\.\\.",""));
								alertAccumulator = "";
							}
						}
						if (line.startsWith("...") && seenForecast) {
							// strange cases where NWS puts a second forecast on the end (e.g.,
							// in the SF Bay region). Just ignore those.
							state = State.END;
							break;
						}
						if (line.startsWith(".") && !line.startsWith("...")) {
							seenForecast = true;
							if (!forecastAccumulator.equals("")) {
								marineDailyForecasts.add(new MarineDailyForecast(forecastDay, forecastAccumulator));
								forecastAccumulator = "";
							}
							int c = line.indexOf("...");
							forecastDay = line.substring(1,c);
							forecastAccumulator = line.substring(c+3).trim() + " ";
						} else if (seenForecast) {
							forecastAccumulator += line.trim() + " ";
						}
					case END:
						break;
					default:
						break;
				}
			} catch (Exception e) {
				Logger.d("Failed to parse forecast, in state " + state.toString());
				Logger.d("forecastAccumulator is " + forecastAccumulator);
				Logger.d("alertAccumulator is " + alertAccumulator);
				Logger.d("dateString is " + dateString);
				Logger.d("line is " + line);
				Logger.d("text is ++++++++++++++++++++");
				Logger.d(text);
				e.printStackTrace();
				result = null;  // TODO REMOVE THIS
			}
		}
		result.setDailyForecast(marineDailyForecasts);
		result.setZoneDescription(zoneDescription);
		return result;
	}
}
