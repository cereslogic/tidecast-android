package com.cereslogic.tidecast.client;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.util.Log;

import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.DateUtils;

public class TideParser {
	public static List<Tide> parseTides(Station station, String data, Date begin, Date end) {
		List<Tide> result = new ArrayList<Tide>();
		String[] lines = data.split("\\n");
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mma", Locale.US);
		//0          1   2     3  4   5   6
		//2010/12/31 Fri 02:48 AM 4.6 140 H
		for (String line : lines) {
			if (line.startsWith("2")) {
				String[] fields = line.split("\\s+");
				try {
					Date day = dayFormat.parse(fields[0]);
  				if (DateUtils.between(day, begin, end)) {
  					Date when = sdf.parse(fields[0] + " " + fields[2] + fields[3]);
  					Float feet = Float.parseFloat(fields[4]);
  					boolean isLowTide = fields[6].equals("L");
  					result.add(new Tide(when, feet, isLowTide));
  				}
				} catch (Exception e) {
					Log.e("Tidecast","Unable to parse tide data.", e);
				}
			}
		}
		return result;
	}
}
