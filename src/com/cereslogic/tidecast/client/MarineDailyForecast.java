package com.cereslogic.tidecast.client;

public class MarineDailyForecast {
	private String day;
	private String text;
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getText() {
		return text;
	}
	public void appendText(String text) {
		this.text += text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public MarineDailyForecast() {
	}
	public MarineDailyForecast(String day, String text) {
		super();
		this.day = day;
		this.text = text;
	}
}
