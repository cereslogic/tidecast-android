package com.cereslogic.tidecast.client;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.cereslogic.tidecast.utils.Logger;

public class LocationClient implements LocationListener {
	public static final Double INVALID_LAT_LONG = 361.0;
	public static final String LATITUDE = "LATITUDE";
	public static final String LONGITUDE = "LONGITUDE";

	private LocationManager mLocationManager;
	private String mBestProvider;
	private Handler mHandler;
	private Location mLocation = null;
	private Timer mTimer;
	
	private static final String[] S = { "Out of Service",	"Temporarily Unavailable", "Available"};
	private static final Long UPDATE_INTERVAL = 400L;
	private static final Long TIMEOUT = UPDATE_INTERVAL + 10000L;
	private static final float DISTANCE_SENSITIVITY = 100.0f;
	
	TimerTask locationTimeout = new TimerTask() {
		public void run() {
		  // We waited for 10 seconds, if the location didn't get set, default to the last known.
		  Logger.d("Timed out waiting for location.");
		  LocationClient outer = LocationClient.this;
		  if (mLocation == null && mBestProvider != null && mLocationManager != null && mHandler != null) {
		  	mLocationManager.removeUpdates(outer);
  	  	mLocation = mLocationManager.getLastKnownLocation(mBestProvider);
		  	Message message = buildMessage(mHandler,mLocation); 
		  	mHandler.sendMessage(message);
		  }
		}
	};
	
	public void cancel() {
		if (mLocationManager != null) {
			mLocationManager.removeUpdates(this);
		}
		if (mTimer != null) {
			mTimer.cancel();
		}
	}
	
	public Location getLocation() {
		return mLocation;
	}
	
	public void getLocationAsync(LocationManager locationManager, final Handler handler) {
		mLocationManager = locationManager;
		mBestProvider = this.getBestLocationProvider();
		mLocationManager.requestLocationUpdates(mBestProvider, UPDATE_INTERVAL, DISTANCE_SENSITIVITY, this);
		mHandler = handler;
		mTimer = new Timer();
		mTimer.schedule(locationTimeout, LocationClient.TIMEOUT);
	}

	public void onLocationChanged(Location location) {
		Logger.d("Location Changed to " + location.getLatitude() + ", " + location.getLongitude());
		if (mTimer != null) {
			mTimer.cancel();
		}
		mLocation = location;
		logLocation(location);
		Message message = buildMessage(mHandler, location); 
		mHandler.sendMessage(message);
		mLocationManager.removeUpdates(this);
	}

	private Message buildMessage(Handler handler, Location location) {
		if (mTimer != null) {
			mTimer.cancel();
		}
		Double lat = INVALID_LAT_LONG;
		Double lon = INVALID_LAT_LONG;
		if (location != null) {
			lat = location.getLatitude();
			lon = location.getLongitude();
		}
		Logger.d("Building a message w/ " + lat + ", " + lon);
		Message message = handler.obtainMessage(); 
		Bundle bundle = new Bundle(); 
		bundle.putDouble(LATITUDE, lat);
		bundle.putDouble(LONGITUDE, lon);
		message.setData(bundle); 
		return message;
	}

	private String getBestLocationProvider() {
		boolean onEmulator = ("sdk".equals( Build.PRODUCT ) || "google_sdk".equals( Build.PRODUCT ));
		if (onEmulator) {
			Logger.d("On Emulator, returning GPS.");
			return LocationManager.GPS_PROVIDER;
		}
		List<String> providers = this.mLocationManager.getAllProviders();
		for (String provider : providers) {
			this.logProvider(provider);
		}
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setSpeedRequired(false);
		String bestProvider = mLocationManager.getBestProvider(criteria, false);
		Logger.d("BEST Provider:" + bestProvider);
		return bestProvider;
	}

	public void onProviderDisabled(String provider) {
		Logger.d("Provider Disabled: " + provider);
//		mBestProvider = this.getBestLocationProvider();
	}

	public void onProviderEnabled(String provider) {
		Logger.d("Provider Enabled: " + provider);
//		mBestProvider = this.getBestLocationProvider();
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		Logger.d("Provider Status Changed: " + provider + ", Status="
				+ S[status] + ", Extras=" + extras);
	}

	private void logProvider(String provider) {
		LocationProvider info = mLocationManager.getProvider(provider);
		Logger.d(info.toString() + "\n\n");
	}

	private void logLocation(Location location) {
		if (location == null) {
			Logger.d("Location: [unknown]");
		} else {
			Logger.d("Location: " + location.toString());
		}
	}
}
