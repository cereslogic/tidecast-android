package com.cereslogic.tidecast.client;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MarineForecast {
	private List<MarineDailyForecast> dailyForecast = new ArrayList<MarineDailyForecast>();
	private String zone;
	private String zoneDescription;
	private Date forecastDate;
	private Date forecastExpires;
	private String synopsis;
	private String alert;
	private String source;

	public List<MarineDailyForecast> getDailyForecast() {
		return dailyForecast;
	}
	public void setDailyForecast(List<MarineDailyForecast> dailyForecast) {
		this.dailyForecast = dailyForecast;
	}
	
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	public String getZoneDescription() {
		return zoneDescription;
	}
	public void setZoneDescription(String zoneDescription) {
		this.zoneDescription = zoneDescription;
	}
	
	public Date getForecastDate() {
		return forecastDate;
	}
	public void setForecastDate(Date forecastDate) {
		this.forecastDate = forecastDate;
	}
	
	public Date getForecastExpires() {
		return forecastExpires;
	}
	public void setForecastExpires(Date forecastExpires) {
		this.forecastExpires = forecastExpires;
	}
	
	public String getSynopsis() {
		return synopsis;
	}
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	
	public String getAlert() {
		return alert;
	}
	public void setAlert(String alert) {
		this.alert = alert;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String toString() {
		String result = "";
		result += "\nSource: " + this.getSource();
		result += "\nZone Description: " + this.getZoneDescription();
		if (this.getForecastDate() != null) {
			result += "\nForecast Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(this.getForecastDate());
		} else {
			result += "\nForecast Date: NULL";
		}
		if (this.getAlert() != null && !this.getAlert().trim().equals("")) {
			result += "\nAlert: " + this.alert;
		}
		result += "\n-- Forecasts ---------";
		for (MarineDailyForecast marineDailyForecast : this.dailyForecast) {
			result += "\nDay: " + marineDailyForecast.getDay();
			result += "\nForecast: " + marineDailyForecast.getText();
		}
		return result;
	}
}
