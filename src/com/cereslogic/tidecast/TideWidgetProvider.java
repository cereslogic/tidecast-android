package com.cereslogic.tidecast;

import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.View;
import android.widget.RemoteViews;

import com.cereslogic.tidecast.client.LocationClient;
import com.cereslogic.tidecast.client.Tide;
import com.cereslogic.tidecast.client.TideForecastClient;
import com.cereslogic.tidecast.client.TidecastClientException;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Logger;
import com.cereslogic.tidecast.utils.Prefs;

public class TideWidgetProvider extends AppWidgetProvider {
	private Context mContext = null;
	private AppWidgetManager mAppWidgetManager = null;
	private ComponentName mThisWidget;
	private Station mStation;
	private RemoteViews mRemoteView;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		Logger.d("In onUpdate.");
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		this.mContext = context;
		this.mAppWidgetManager = appWidgetManager;
		this.mThisWidget = new ComponentName(context, TideWidgetProvider.class);
		this.mRemoteView = new RemoteViews(context.getPackageName(), R.layout.widget);
		go();
	}
	
	@Override 
	public void onReceive(Context context, Intent intent) {
		Logger.d("in onReceive, got action '" + intent.getAction() + "'");
		
		mContext = context;
		mAppWidgetManager = AppWidgetManager.getInstance(context);
		mThisWidget = new ComponentName(context, TideWidgetProvider.class); 
		mRemoteView = new RemoteViews(context.getPackageName(), R.layout.widget);
		go();
	}
	
	private void go() {
		try {
			Prefs prefs = new Prefs(mContext);
			if (prefs.isAutoWidget()) {
				Logger.d("isAutoWidget ... need to lookup the location.");
				LocationClient lc = new LocationClient();
				LocationManager locationManager = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);
				lc.getLocationAsync(locationManager, locationHandler);
			} else {
				Logger.d("Not isAutoWidget ... using location " + prefs.getWidgetStation());
				Dao stations = Dao.getInstance(mContext);
				this.mStation = stations.getStation(prefs.getWidgetStation());
				getTides();
			}
		} catch (Throwable t) {
			Logger.e("Caught an exception in widget go... ", t);
		}
	}
	
	private void getTides() {
		setWidgetText("Updating...");
	  Calendar cal1 = new GregorianCalendar();
	  cal1.add(Calendar.DATE, -1);
	  Calendar cal2 = new GregorianCalendar();
	  cal2.add(Calendar.DATE, 1);
	  if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) {
	  	// Can't span a year easily (two API calls), just punt and do it all in one year.
	  	cal1 = new GregorianCalendar();
	  	cal2 = new GregorianCalendar();
	  	cal2.add(Calendar.DATE, 1);
	  	cal2.add(Calendar.DATE, 2);
	  }
	  new FetchTidesTask().execute(new TideParams(this.mStation, cal1.getTime(), cal2.getTime()));
	}
	
	// Location handler. Gymnastics w/ WeakReferences to prevent memory leaks 
	// (Activities with handlers can't be GC'd because message queue still has a 
	// reference to the handler).
	//
	private static final class HandlerExtension extends Handler {
		private final SoftReference<TideWidgetProvider> mTideWidgetProvider;
		public HandlerExtension(TideWidgetProvider tideWidgetProvider) {
			mTideWidgetProvider = new SoftReference<TideWidgetProvider>(tideWidgetProvider);
		}
		
		public void handleMessage(final Message msg) {
			Logger.d("Widget got the location message.");
			Double lat = msg.getData().getDouble(LocationClient.LATITUDE);
			Double lon = msg.getData().getDouble(LocationClient.LONGITUDE);
			TideWidgetProvider tideWidgetProvider = mTideWidgetProvider.get();
			if (tideWidgetProvider != null) {
				Context context = tideWidgetProvider.mContext;
				if (lat.equals(LocationClient.INVALID_LAT_LONG) && lon.equals(LocationClient.INVALID_LAT_LONG)) {
					Logger.d("Location is unknown.");
					tideWidgetProvider.setWidgetText(context.getString(R.string.couldnt_determine_location));
				} else {
					tideWidgetProvider.setWidgetText(context.getString(R.string.finding_closest_station));
					Logger.d("Location is " + lat + " , " + lon);
					Station closest = Dao.getInstance(context).findClosestStation(lon,lat);
					if (closest != null) {
						Logger.d("Closest station is " + closest.getPlace());
						tideWidgetProvider.mStation = closest;
						tideWidgetProvider.getTides();
					} else {
						tideWidgetProvider.setWidgetText(context.getString(R.string.couldnt_determine_closest_station));
						Logger.d("Problem identifying the closest station.");
					}
				}
			} else {
				Logger.d("Lost our reference to the TideWidgetProvider.");
			}
			mTideWidgetProvider.clear();
		}
	}
	private final Handler locationHandler = new HandlerExtension(this);
	
	// Utility struct for passing params to async task
	private class TideParams {
		public Station station;
		public Date startDate;
		public Date endDate;
		public TideParams(final Station station, final Date startDate, final Date endDate) {
			this.station = station;
			this.startDate = startDate;
			this.endDate = endDate;
		}
	}
	
	private class FetchTidesTask extends AsyncTask</*Params*/ TideParams, /* Progress */ Void, /* Result */ List<Tide>> {
    protected List<Tide> doInBackground(TideParams... tideParams) {
    	TideForecastClient tideForecastClient = new TideForecastClient();
    	List<Tide> result = new ArrayList<Tide>();
  	  try {
				result = tideForecastClient.getTides(tideParams[0].station, tideParams[0].startDate, tideParams[0].endDate);
			} catch (TidecastClientException e) {
				setWidgetText(tideParams[0].station.getPlace());
			}
			return result;
    }
    
    protected void onPostExecute(List<Tide> tides) {
			Date now = new Date();
			List<Tide> keepers = new ArrayList<Tide>();
			Tide mostRecent = null;
			// Get the most recent tide, and the next three tides after the current time.
			for (Tide tide : tides) {
				if (tide.getWhen().before(now)) {
					mostRecent = tide;
				} else {
					if (keepers.size() < 3) {
						keepers.add(tide);
					}
				}
			}
			keepers.add(0, mostRecent);
			drawTides(keepers);
    }
	}

	private void drawTides(List<Tide> tides) {
		Logger.d("in drawTides.");
		mRemoteView.setViewVisibility(R.id.divider0102, View.INVISIBLE);
		mRemoteView.setViewVisibility(R.id.divider0203, View.INVISIBLE);
		mRemoteView.setViewVisibility(R.id.divider0304, View.INVISIBLE);
		setWidgetText(this.mStation.getPlace());

		int i = 0;
		Tide previousTide = null;
		for (Tide tide : tides) {
			i++;
			drawOneTide(tide,i);
			if (previousTide != null) {
				Calendar cal1 = new GregorianCalendar(); cal1.setTime(previousTide.getWhen());
				Calendar cal2 = new GregorianCalendar(); cal2.setTime(tide.getWhen());
				Logger.d("cal1 date: " + cal1.get(Calendar.DATE) +", cal2 date: " + cal2.get(Calendar.DATE) + ", i: " + i);
				if (cal1.get(Calendar.DATE) != cal2.get(Calendar.DATE)){
					switch (i) {
						case 2:
							mRemoteView.setViewVisibility(R.id.divider0102, View.VISIBLE);
							break;
						case 3:
							mRemoteView.setViewVisibility(R.id.divider0203, View.VISIBLE);
							break;
						case 4:
							mRemoteView.setViewVisibility(R.id.divider0304, View.VISIBLE);
							break;
					}
				}
			}
			previousTide = tide;
		}	

		if (mStation != null) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
	   	intent.putExtra("station", ((Parcelable)mStation));
	    intent.putExtra("day", new Date());
	    intent.setComponent(new ComponentName("com.cereslogic.tidecast","com.cereslogic.tidecast.activities.ResultContainerActivity"));
	    PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
	    mRemoteView.setOnClickPendingIntent(R.id.widget_layout, pendingIntent);
		}

		mAppWidgetManager.updateAppWidget(mThisWidget, mRemoteView);
		Logger.d("done drawTides.");
	}
	
	private void drawOneTide(Tide tide, int number) {
		int hideId = 0, altLayoutId = 0, textId = 0, layoutId = 0;
		switch(number) {
			case 1:
				hideId = R.id.day01_mid;
				textId = tide.isHighTide() ? R.id.text_day01_high : R.id.text_day01_low;
				layoutId = tide.isHighTide() ? R.id.day01_high : R.id.day01_low;
				altLayoutId = tide.isHighTide() ? R.id.day01_low : R.id.day01_high;
				break;
			case 2:
				hideId = R.id.day02_mid;
				textId = tide.isHighTide() ? R.id.text_day02_high : R.id.text_day02_low;
				layoutId = tide.isHighTide() ? R.id.day02_high : R.id.day02_low;
				altLayoutId = tide.isHighTide() ? R.id.day02_low : R.id.day02_high;
				break;
			case 3:
				hideId = R.id.day03_mid;
				textId = tide.isHighTide() ? R.id.text_day03_high : R.id.text_day03_low;
				layoutId = tide.isHighTide() ? R.id.day03_high : R.id.day03_low;
				altLayoutId = tide.isHighTide() ? R.id.day03_low : R.id.day03_high;
				break;
			case 4:
				hideId = R.id.day04_mid;
				textId = tide.isHighTide() ? R.id.text_day04_high : R.id.text_day04_low;
				layoutId = tide.isHighTide() ? R.id.day04_high : R.id.day04_low;
				altLayoutId = tide.isHighTide() ? R.id.day04_low : R.id.day04_high;
				break;
		}		
		
		mRemoteView.setViewVisibility(hideId, View.GONE);
		mRemoteView.setViewVisibility(altLayoutId, View.GONE);
		mRemoteView.setViewVisibility(layoutId, View.VISIBLE);
		String time = new SimpleDateFormat("h:mma", Locale.US).format(tide.getWhen()).toLowerCase(Locale.US);
		time = time.substring(0,time.length() - 1);
		mRemoteView.setTextViewText(textId,time);
	}

	protected void setWidgetText(String text) {
		mRemoteView.setTextViewText(R.id.widget_place, text);		
		mAppWidgetManager.updateAppWidget(mThisWidget, mRemoteView);
	}
}
