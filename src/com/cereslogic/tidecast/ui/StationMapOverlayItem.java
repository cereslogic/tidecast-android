package com.cereslogic.tidecast.ui;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.cereslogic.tidecast.utils.Logger;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class StationMapOverlayItem extends ItemizedOverlay<OverlayItem> {
	 private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	 private Context context;

	 public StationMapOverlayItem(Drawable defaultMarker, Context context) {
		 super(boundCenterBottom(defaultMarker));
		 this.context = context;
	 }

	 public void addOverlay(OverlayItem overlay) {
		 overlays.add(overlay);
		// populate();
	 }
	 
	 public void populateNow() {
		 populate();
	 }
	 
	 @Override
	 protected OverlayItem createItem(int i) {
		 return overlays.get(i);
	 }
	 
	 @Override
	 public int size() {
		 return overlays.size();
	 }
	 
	 @Override
	 protected boolean onTap(int index) {
		 Logger.d("In here? Index " + index);
		 OverlayItem item = overlays.get(index);
		 AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		 dialog.setTitle(item.getTitle());
		 dialog.setMessage(item.getSnippet());
		 dialog.show();
		 return true;
	 }
}
