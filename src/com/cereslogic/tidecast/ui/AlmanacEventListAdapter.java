package com.cereslogic.tidecast.ui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.data.ResultListActionItem;
import com.cereslogic.tidecast.data.ResultListAlmanacEvent;
import com.cereslogic.tidecast.data.ResultListItem;
import com.cereslogic.tidecast.utils.Logger;

public class AlmanacEventListAdapter extends BaseAdapter {

	private final Context context; 
	private final List<ResultListItem> items; 
	
	public AlmanacEventListAdapter(Context context, List<ResultListItem> items) {
		this.context = context; 
		this.items = items;
	}

	public int getCount() {
		return this.items.size();
	}

	public Object getItem(int pos) {
		return this.items.get(pos);
	}

	public long getItemId(int pos) {
		return pos;
	}
	
	@Override
	public boolean isEnabled(int position) {
		boolean result = false;
		ResultListItem item = (ResultListItem)getItem(position);
		if (item instanceof ResultListAlmanacEvent) {
			result = ((ResultListAlmanacEvent)item).isClickable();
		}
		return result;
	}

	public View getView(int position, View ignoredConvertView, ViewGroup parent) {
		View v = ignoredConvertView;
    ResultListItem item = this.items.get(position);
    if (item != null) {
    	if (item instanceof ResultListAlmanacEvent) {
  			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  			v = vi.inflate(R.layout.almanac_event_list_item, null);
    		ResultListAlmanacEvent ae = (ResultListAlmanacEvent)item;
    		TextView label = (TextView)v.findViewById(R.id.label);
    		if (label != null) {
    			label.setText(ae.getLabel());
    		}
    		TextView value = (TextView)v.findViewById(R.id.value);
    		if (value != null) {
    			value.setText(ae.getValue());
    		}
    		ImageView icon = (ImageView)v.findViewById(R.id.icon);
    		icon.setImageBitmap(ae.getIcon());
    	} else if (item instanceof ResultListActionItem) {
  			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  			v = vi.inflate(R.layout.action_item_list_item, null);
    		Button value = (Button)v.findViewById(R.id.action_name);
    		if (value != null) {
    			ResultListActionItem ai = (ResultListActionItem)item;
    			value.setText(ai.getAction());
    			value.setEnabled(ai.isActionEnabled());
    		}
    		value = (Button)v.findViewById(R.id.alt_action_name);
    		if (value != null) {
    			ResultListActionItem ai = (ResultListActionItem)item;
    			value.setText(ai.getAltAction());
    			value.setEnabled(ai.isAltActionEnabled());
    		} 
    	} else {
    		Logger.e("WTF, this doesn't belong in here!");
    		Logger.e("It's a " + item.getClass().getCanonicalName());
    	}
    }
    return v;
	}	
}
