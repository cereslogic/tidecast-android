package com.cereslogic.tidecast.ui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.persist.IdObject;

public class MenuListAdapter extends BaseAdapter {

	private final Context mContext; 
	private List<? extends IdObject> mChoices; 
	
	public MenuListAdapter(Context context, List<? extends IdObject> choices) {
		this.mContext = context; 
		this.mChoices = choices;
	}

	public int getCount() {
		return this.mChoices.size();
	}

	public Object getItem(int pos) {
		return this.mChoices.get(pos);
	}

	public long getItemId(int pos) {
		return pos;
	}
	
	public void setChoices(List<? extends IdObject> choices) {
		this.mChoices = choices;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		 View v = convertView;
     if (v == null) {
    	 LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	 v = vi.inflate(R.layout.menu_list_item, null);
     }
     String string = this.mChoices.get(position).getText();
     if (string != null) {
    	 TextView tt = (TextView) v.findViewById(R.id.text);
    	 if (tt != null) {
    		 tt.setText(string);
    	 }
     }
     return v;
	}
}
