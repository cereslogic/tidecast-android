package com.cereslogic.tidecast.ui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cereslogic.tidecast.R;

public class TideCastListAdapter extends BaseAdapter {

	private final Context context; 
	private final List<String> choices; 
	
	public TideCastListAdapter(Context context, List<String> choices) {
		this.context = context; 
		this.choices = choices;
	}

	public int getCount() {
		return this.choices.size();
	}

	public Object getItem(int pos) {
		return this.choices.get(pos);
	}

	public long getItemId(int pos) {
		return pos;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		 View v = convertView;
     if (v == null) {
    	 LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	 v = vi.inflate(R.layout.tidecast_list_item, null);
     }
     String string = this.choices.get(position);
     if (string != null) {
    	 TextView tt = (TextView) v.findViewById(R.id.text);
    	 if (tt != null) {
    		 tt.setText(string);
    	 }
     }
     return v;
	}
}
