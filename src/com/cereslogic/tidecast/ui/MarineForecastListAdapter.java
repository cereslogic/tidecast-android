package com.cereslogic.tidecast.ui;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.cereslogic.tidecast.R;
import com.cereslogic.tidecast.client.MarineDailyForecast;
import com.cereslogic.tidecast.data.ResultListActionItem;
import com.cereslogic.tidecast.data.ResultListForecastFooter;
import com.cereslogic.tidecast.data.ResultListForecastHeader;
import com.cereslogic.tidecast.utils.Logger;

public class MarineForecastListAdapter extends BaseAdapter {

	private final Context context; 
	private final List<Object> items; 
	
	public MarineForecastListAdapter(Context context, List<Object> items) {
		this.context = context; 
		this.items = items;
	}

	public int getCount() {
		return this.items.size();
	}

	public Object getItem(int pos) {
		return this.items.get(pos);
	}

	public long getItemId(int pos) {
		return pos;
	}
	
	public View getView(int position, View ignoredConvertView, ViewGroup parent) {
		View v = ignoredConvertView;
    Object item = this.items.get(position);
    if (item != null) {
    	if (item instanceof ResultListForecastHeader) {
    		ResultListForecastHeader header = (ResultListForecastHeader)item;
  			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  			v = vi.inflate(R.layout.forecast_header_list_item, null);
  			TextView text = (TextView)v.findViewById(R.id.forecast_alert);
  			if (text != null) {
  				if (header.getForecastAlert() != null && !header.getForecastAlert().trim().equals("")) {
  					text.setVisibility(View.VISIBLE);
  					text.setText(header.getForecastAlert());
  				}
  			}
  			text = (TextView)v.findViewById(R.id.forecast_date);
  			if (text != null) {
  				text.setText(new SimpleDateFormat("EEEE, MMMM d h:ma zzz",Locale.US).format(header.getForecastDate()));
  			}
    	} else if (item instanceof ResultListForecastFooter) {
    		ResultListForecastFooter footer = (ResultListForecastFooter)item;
  			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  			v = vi.inflate(R.layout.forecast_footer_list_item, null);
  			TextView text = (TextView)v.findViewById(R.id.forecast_zone_description);
  			if (text != null) {
  				text.setText(footer.getZoneDescription());
  			}
    	} else if (item instanceof MarineDailyForecast) {
    		MarineDailyForecast forecast = (MarineDailyForecast)item;
   			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		v = vi.inflate(R.layout.forecast_daily_list_item, null);
  			TextView text = (TextView)v.findViewById(R.id.forecast_for);
  			if (text != null) {
  				text.setText(forecast.getDay());
  			}
  			text = (TextView)v.findViewById(R.id.forecast_content);
  			if (text != null) {
  				text.setText(forecast.getText());
  			}
    	} else if (item instanceof ResultListActionItem) {
   			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		v = vi.inflate(R.layout.action_item_list_item, null);
    		Button value = (Button)v.findViewById(R.id.action_name);
    		if (value != null) {
    			ResultListActionItem ai = (ResultListActionItem)item;
    			value.setText(ai.getAction());
    			value.setEnabled(ai.isActionEnabled());
    		}
    		value = (Button)v.findViewById(R.id.alt_action_name);
    		if (value != null) {
    			ResultListActionItem ai = (ResultListActionItem)item;
    			value.setText(ai.getAltAction());
    			value.setEnabled(ai.isAltActionEnabled());
    		} 
    	} else {
    		Logger.e("WTF, this doesn't belong in here!");
    		Logger.e("It's a " + item.getClass().getCanonicalName());
    	}
    }
    return v;
	}	
}
