package com.cereslogic.tidecast.test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.test.AndroidTestCase;

import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Logger;
import com.cereslogic.tidecast.utils.StringUtils;

public class TideClientTests extends AndroidTestCase {
	// private final static String URL_BASE = "http://tidesandcurrents.noaa.gov/cgi-bin/get_predictions.cgi?size=small";   OLD
	//private final static String URL_BASE = "http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT&timelength=daily&dataUnits=1&timeUnits=2&pageview=dayly&print_download=true";
	private final static String URL_BASE = "http://140.90.78.215/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT&timelength=daily&dataUnits=1&timeUnits=2&pageview=dayly&print_download=true";
	
	
	// FAIL
	// curl "http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT&timelength=daily&dataUnits=1&timeUnits=2&pageview=dayly&print_download=true&StationId=8638476&TimeOffsetHigh=-376&TimeOffsetLow=-399&HeightOffsetHigh=*1.02&HeightOffsetLow=*0.83&bdate=2011
	// curl "http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT\&StationId=8638476\&bdate=20110101\&timelength=daily\&dataUnits=1\&HeightOffsetHigh=*1.02\&HeightOffsetLow=*0.83\&TimeOffsetLow=-399\&TimeOffsetHigh=-376&pageview=dayly&print_download=true

	
	// OK
	// curl  http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?datatype=Annual+TXT\&Stationid=8418137\&bdate=20110106\&timelength=daily\&dataUnits=1\&HeightOffsetLow=*1.06\&HeightOffsetHigh=*+1.01\&TimeOffsetLow=4\&TimeOffsetHigh=1\&pageview=dayly\&print_download=true
	
	//private NoaaTideClient ntc = new NoaaTideClient();
	private final static int CONNECTION_TIMEOUT_MILLISECONDS = 10000;
	private final static int SOCKET_TIMEOUT_MILLISECONDS = 10000;

//	public void testMarineForecastUrls() throws Throwable {
//		Logger.d("++++++++++++++++ Loading up the stations, this takes a while...");
//		List<Integer> ids = Stations.getInstance(getContext()).getAllStationIds();
//		Logger.d("...done!");
//		int count = ids.size();
//		int i = 0;
//		Station station = null;
//		for (Integer id : ids) {
//			if (id < 2600) {
//				continue;
//			}
//			i++;
//			station = Stations.getInstance(getContext()).getStation(id);
//			Logger.d("++++++++++++++++ ON " + id + "/" + count + " ++++++++++++++++");
//			Logger.d("Station " + id + " " + station.getPlace());
// 			String url = station.getMarineForecastUrl();
// 			HttpGet httpGet = new HttpGet(url);
// 			Logger.d("Executing HTTP call to " + url);
// 			
//			final ResponseHandler<String> responseHandler =
//				new ResponseHandler<String>() {	
//					public String handleResponse(HttpResponse response) {
//						Logger.d("In ResponseHandler<String>.handleResponse...");
//						HttpEntity entity = response.getEntity(); 
//						
//						String result = null;
//						try {
//							//Assert.assertTrue(1==2);
//							result = StringUtils.inputStreamToString(entity.getContent());
//							Logger.d("+++++++++++++++++++++++ result:" + result);
//							Logger.d("+++++++++++++++++++++++ result length = " + result.length());
//							Logger.d("+++++++++++++++++++++++ status = " + response.getStatusLine().getStatusCode());
//							Assert.assertNotNull(result);
//							Assert.assertTrue(result.length() > 500);
//							Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);
//		        } catch (IOException e) { 
//		        	Logger.e("!!!!!!!!!!!!!!!!!!!!!!!!! Caught an error contacting NOAA.",e);
//		        	Assert.fail();
//		        	// what to do now?
//		        }	
//		        return result;
//		    	}
//		    };
//
// 			
// 			HttpParams httpParams = new BasicHttpParams();
// 			HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT_MILLISECONDS);
// 			HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT_MILLISECONDS);
// 			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
// 			httpClient.execute(httpGet, responseHandler);
//			//ntc.getTidesAsync(station,new Date(),this.handler);
//			station = null;
//			Thread.sleep(3000);
//		}
//	}
//	
	
	public void testUrls() throws Throwable {
		Logger.d("++++++++++++++++ Loading up the stations, this takes a while...");
		List<Integer> ids = Dao.getInstance(getContext()).getAllStationIds();
		Logger.d("...done!");
		int count = ids.size();
		int i = 0;
		Station station = null;
		for (Integer id : ids) {
			i++;
//			if (i <= 2832 || i == 372 || i == 812 || i == 1134 || i == 1135 || i == 1138 || i == 2705 || i == 2706 || i == 2707
//					|| i == 2708 || i == 2709 || i == 2710 || i == 2711 || i == 2715 || i == 2716 || i == 2717 || i == 2718 || i == 2719 
//					|| i == 2720 || i == 2721 || i == 2722 || i == 2723 || i == 2724 || i == 2725 || i == 2727 || i == 2729 || i == 2730
//					|| i == 2731 || i == 2735 || i == 2736 || i == 2737 || i == 2738 || i == 2739 || i == 2740 || i == 2741 || i == 2755
//					|| i == 2767 || i == 2768 || i == 2769 || i == 2831 || i == 2832) {
//				continue; 
//			}
			station = Dao.getInstance(getContext()).getStation(id);
			if (!station.isEnabled() || i < 1130) {
				continue;
			}
			
			Logger.d("++++++++++++++++ ON " + i + "/" + count + " ++++++++++++++++");
			Logger.d("Station " + id + " " + station.getPlace());
			
 			//String url = URL_BASE + station.getApiUrlParams() + "&bdate=" + new SimpleDateFormat("yyyy").format(d) + "0101";
			String url = URL_BASE + station.getApiUrlParams() + "&bdate=20120101";
 			HttpGet httpGet = new HttpGet(url);
 			Logger.d("Executing HTTP call to " + url);
 			
			final ResponseHandler<String> responseHandler =
				new ResponseHandler<String>() {	
					public String handleResponse(HttpResponse response) {
						Logger.d("In ResponseHandler<String>.handleResponse...");
						HttpEntity entity = response.getEntity(); 
						
						String result = null;
						try {
							//Assert.assertTrue(1==2);
							result = StringUtils.inputStreamToString(entity.getContent());
							Logger.d("+++++++++++++++++++++++ result: " + result);
							Logger.d("+++++++++++++++++++++++ result length = " + result.length());
							Logger.d("+++++++++++++++++++++++ status = " + response.getStatusLine().getStatusCode());
							Assert.assertNotNull(result);
							Assert.assertTrue(result.length() > 20000);
							Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);
		        } catch (IOException e) { 
		        	
		        	Logger.e("!!!!!!!!!!!!!!!!!!!!!!!!! Caught an error contacting NOAA.",e);
		        	Assert.fail();
		        	// what to do now?
		        }	
		        return result;
		    	}
		    };

 			
 			HttpParams httpParams = new BasicHttpParams();
 			HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT_MILLISECONDS);
 			HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT_MILLISECONDS);
 			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
 			httpClient.execute(httpGet, responseHandler);
			//ntc.getTidesAsync(station,new Date(),this.handler);
			station = null;
			Thread.sleep(3000);
		}
	}
}
