package com.cereslogic.tidecast.test;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import android.test.AndroidTestCase;

import com.cereslogic.tidecast.client.MarineForecast;
import com.cereslogic.tidecast.client.MarineForecastParser;
import com.cereslogic.tidecast.utils.Logger;

public class ForecastParseTest extends AndroidTestCase {
	public void testParser() {
		String text = null;
		try {
			text = loadTestForecast("forecast07.txt");
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		MarineForecast marineForecast = MarineForecastParser.parseMarineForecast(text);
		assertNotNull(marineForecast.getSource());
		Logger.d("%%% FORECAST:\n" + marineForecast.toString());
	}
	
	private String loadTestForecast(String filename) throws Exception {
		InputStream input = getContext().getAssets().open("test-forecasts/" + filename);
		Reader reader = new InputStreamReader(input, "UTF-8");
		
		StringBuffer fileData = new StringBuffer(1000);
    //BufferedReader reader = new BufferedReader(new FileReader("../../../../etc/test-forecasts/" + filename));
    char[] buf = new char[1024];
    int numRead=0;
    while((numRead=reader.read(buf)) != -1){
    	String readData = String.valueOf(buf, 0, numRead);
      fileData.append(readData);
      buf = new char[1024];
    }
    reader.close();
    return fileData.toString();
  }
}
