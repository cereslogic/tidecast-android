package com.cereslogic.tidecast.test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.test.AndroidTestCase;

import com.cereslogic.tidecast.client.MarineForecast;
import com.cereslogic.tidecast.client.MarineForecastClient;
import com.cereslogic.tidecast.persist.Dao;
import com.cereslogic.tidecast.persist.Station;
import com.cereslogic.tidecast.utils.Logger;

public class MarineForecastClientTests extends AndroidTestCase {
	public void testOneForecast() throws Throwable {
		Station station = Dao.getInstance(getContext()).getStation(1728);
		Logger.d(" ++++++++++++++++++++++ Fetching for zone " + station.getZone() + ", using Station " + station.getId() + " ++++++++++++++++++++++++++");
		MarineForecastClient mfc = new MarineForecastClient();
		MarineForecast mf = mfc.getForecast(station);
		Logger.d(mf.toString());
		Logger.d(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
	}
	
	public void testForecasts() throws Throwable {
		Logger.d("++++++++++++++++ Loading up the stations, this takes a while...");
		List<Integer> ids = Dao.getInstance(getContext()).getAllStationIds();
		Set<String> forecastUrls = new HashSet<String>();
		int i = 0;
		Station station = null;
		MarineForecastClient mfc = new MarineForecastClient();
		for (Integer id : ids) {
			i++;
			station = Dao.getInstance(getContext()).getStation(id);
			if (! forecastUrls.contains(station.getMarineForecastUrl())) {
				forecastUrls.add(station.getMarineForecastUrl());
				Logger.d(" ++++++++++++++++++++++ Fetching for zone " + station.getZone() + ", using Station " + station.getId() + " ++++++++++++++++++++++++++");
				MarineForecast mf = mfc.getForecast(station);
				Logger.d(mf.toString());
				Logger.d(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
			}
		}
		// Don't DoS NOAA.
		Thread.sleep(5000);
	}
}
